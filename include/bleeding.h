#ifndef BLEEDING_H_
#define BLEEDING_H_

#ifdef DLL_IMPLEMENT   
#define DLL_API __declspec(dllexport)   
#else   
#define DLL_API __declspec(dllimport)   
#endif

#include <opencv2/opencv.hpp>
#include "base_struct.h"

namespace AirlookStitching {
  DLL_API cv::Mat Bleeding(const std::vector<ImageInfo>& image_infos);
  DLL_API cv::Mat ImageRotate(const cv::Mat& img, double angle);
  DLL_API void LoadImageInfoFromXml(const std::string& xml_path, std::vector<Image>& images);
  DLL_API cv::Mat ImageWarpAffine(const cv::Mat& image, const cv::Mat& M);
  DLL_API cv::Mat ImageWarpPerspective(const cv::Mat& image, const cv::Mat& M);
  DLL_API cv::Mat AddAlphaChannel(const cv::Mat& im);
  DLL_API cv::Mat DelAlphaChannel(const cv::Mat& im);
  DLL_API cv::Mat GetAlphaChannel(const cv::Mat& im);
  DLL_API void LoadImageInfoFromOrb(const std::string& key_frame_path, std::vector<Image>& images);
  //void ImageStitching(std::vector<>);
}
#endif