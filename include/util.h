#ifndef AIRLOOK_UTIL_H_
#define AIRLOOK_UTIL_H_
#include <opencv2/opencv.hpp>
#include <Eigen/Eigen>
#ifdef DLL_IMPLEMENT   
#define DLL_API __declspec(dllexport)   
#else   
#define DLL_API __declspec(dllimport)   
#endif

namespace AirLookUAV {
  inline cv::Point2d ApplyProjection9(const cv::Point2d& point, const cv::Mat& H);
  inline cv::Point2d ApplyProjection6(const cv::Point2d& point, const cv::Mat& H);
  DLL_API cv::Mat ImageRotate(const cv::Mat& img, double angle);
  DLL_API cv::Mat ImageWarpAffine(const cv::Mat& image, const cv::Mat& M);
  DLL_API cv::Mat ImageWarpPerspective(const cv::Mat& image, const cv::Mat& M);
  DLL_API cv::Mat AddAlphaChannel(const cv::Mat& im);
  DLL_API cv::Mat AddAlphaChannel(const cv::Mat& im, const cv::Mat& alpha);
  DLL_API cv::Mat DelAlphaChannel(const cv::Mat& im);
  DLL_API cv::Mat GetAlphaChannel(const cv::Mat& im);
  DLL_API double rad(double d);
  DLL_API double deg(double x);
  void DLL_API SchmidtOrthogonalization(cv::Mat &mat);
#define PI 3.1415926535898

  DLL_API Eigen::Vector2d ComputeThatLonLat(double lon, double lat, double brng, double dist);

}
#endif
