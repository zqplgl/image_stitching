#ifndef TIMER_H_
#define TIMER_H_

#include <chrono>
#ifdef DLL_IMPLEMENT   
#define DLL_API __declspec(dllexport)   
#else   
#define DLL_API __declspec(dllimport)   
#endif

class DLL_API Timer {
 public:
  Timer();

  void Start();
  void Restart();
  void Pause();
  void Resume();
  void Reset();

  double ElapsedMicroSeconds() const;
  double ElapsedSeconds() const;
  double ElapsedMinutes() const;
  double ElapsedHours() const;

 private:
  bool started_;
  bool paused_;
  std::chrono::high_resolution_clock::time_point start_time_;
  std::chrono::high_resolution_clock::time_point pause_time_;
};

#endif
