#ifndef AIRLOOK_IMAGE_STITCHING_H_
#define AIRLOOK_IMAGE_STITCHING_H_

#ifdef DLL_IMPLEMENT   
#define DLL_API __declspec(dllexport)   
#else   
#define DLL_API __declspec(dllimport)   
#endif
#include <opencv2/opencv.hpp>
#include <Eigen/Eigen>
#include <memory>
#include <unordered_map>
#include <opencv2/xfeatures2d.hpp>

namespace AirLookUAV {
  struct ImageInfo {
    cv::Mat image;
    double timestamp;
    int id;
    Eigen::Vector4d quat;
    Eigen::Matrix3d R;
    Eigen::Vector3d t;
    Eigen::Vector3d pos;
    Eigen::Matrix3d K;
    Eigen::Vector3d center;
    int air_strip;

    cv::Mat mask;
    cv::Mat rotate_image;
    std::vector<cv::KeyPoint> key_points;
    cv::Mat descriptors;
    cv::Mat homo;
  };

  struct ImageStitchingInfo {
    cv::Mat im;
    cv::Point2d min_point;
    int id;
    cv::Mat mask;
    cv::Mat bleeding_mask;
    cv::Rect rect;
    cv::Point2d corners[4];
    Eigen::Vector3d pos;
  };

  struct Point4Pos {
    Eigen::Vector2d left_top;
    Eigen::Vector2d right_top;
    Eigen::Vector2d right_bottom;
    Eigen::Vector2d left_bottom;
  };

  class DLL_API ImageStitching {
  public:
    ImageStitching();
    cv::Mat Stitching(std::shared_ptr<ImageInfo>& image_info, Point4Pos& point4_pos);
    
    cv::Mat Stitching(std::vector<std::shared_ptr<ImageInfo>>& image_infos, Point4Pos& point4_pos);

    bool AddImageInfo(std::shared_ptr<ImageInfo>& image_infos);
    
    void LoadImageInfoFromXml(const std::string& xml_path, std::vector<std::shared_ptr<ImageInfo>>& image_infos);

    void UpdateHomo(std::shared_ptr<ImageInfo>& image_info);

    cv::Mat Bleeding(const std::vector<std::shared_ptr<ImageInfo>>& image_infos, Point4Pos& point4_pos);

    cv::Mat Bleeding();

    void CalculatePoint4Pos(const cv::Size& image_size, Point4Pos& point4_pos);

    cv::Mat Bleeding(Point4Pos& point4_pos);

    void CalcuPoint4Pos(const cv::Size& image_size, const Eigen::Matrix3d& K,
      const std::pair<Eigen::Vector2d, Eigen::Vector3d>& point2pos, Point4Pos& point4_pos);

	  void GenerateTiff(const std::string& png_path, const Point4Pos& point4_pos, const std::string& tiff_path);

    void GenerateTiff(const std::string& png_path, const std::string& tiff_path);

  private:
    cv::Mat FindHomo(const ImageInfo&image_info1, const ImageInfo& image_info2, bool& flag);
    int GetNearestImageId(const Eigen::Vector3d& center);
    void GenerateImageStitchingInfo(const std::shared_ptr<ImageInfo>& image_info, ImageStitchingInfo& image_stitching_info);
  private:
    int strip_id_;
    std::vector<std::shared_ptr<ImageInfo>> image_infos_;
    cv::Ptr<cv::ORB> orb_;
    cv::Ptr<cv::xfeatures2d::SURF> surf_;
    cv::Ptr<cv::xfeatures2d::SIFT> sift_;
    cv::Ptr<cv::BFMatcher> bf_matcher_;
    std::unordered_map<int, std::shared_ptr<ImageInfo>> image_infos_map_;
    cv::Point2d global_min_point_;
    cv::Point2d global_max_point_;
    std::vector<ImageStitchingInfo> image_stitching_infos_;
    std::map<int, Eigen::Vector3d> prev_strip_centers_;
    std::map<int, Eigen::Vector3d> current_strip_centers_;

    cv::Mat M_;
    cv::Mat M1_;
  };
}

#endif