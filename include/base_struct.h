#ifndef BASE_STRUCT_H_
#define BASE_STRUCT_H_
#include<opencv2/opencv.hpp>
#include <memory>
#include <Eigen/Eigen>
#include <thread>

namespace AirlookStitching {
  struct ImageInfo {
    cv::Mat image;
    cv::Mat mask;
    std::vector<cv::KeyPoint> key_points;
    cv::Mat descriptors;
    cv::Mat homo; 
    int id;
    cv::Point trans;
    int flag = false;
  };
  struct ImageStitchingInfo {
    cv::Mat im;
    cv::Point2d min_point;
    cv::Mat mask;
    cv::Rect rect;
	cv::Point2d corners[4];
  };

  struct Image {
    int id;
    std::string image_path;
    cv::Mat im;
    cv::Mat R;
    cv::Mat c;
    Eigen::Vector3d center;
    cv::Mat K;
    std::vector<double> dist_coeffs;
  };
}
#endif
