#include "util.h"

namespace AirLookUAV {
  inline cv::Point2d ApplyProjection9(const cv::Point2d& point, const cv::Mat& H) {
    double x = (H.at<double>(0, 0)*point.x + H.at<double>(0, 1)*point.y + H.at<double>(0, 2)) /
      (H.at<double>(2, 0)*point.x + H.at<double>(2, 1)*point.y + H.at<double>(2, 2));

    double y = (H.at<double>(1, 0)*point.x + H.at<double>(1, 1)*point.y + H.at<double>(1, 2)) /
      (H.at<double>(2, 0)*point.x + H.at<double>(2, 1)*point.y + H.at<double>(2, 2));

    return cv::Point2d(x, y);
  }

  inline cv::Point2d ApplyProjection6(const cv::Point2d& point, const cv::Mat& H) {
    double x = (H.at<double>(0, 0)*point.x + H.at<double>(0, 1)*point.y + H.at<double>(0, 2));
    double y = (H.at<double>(1, 0)*point.x + H.at<double>(1, 1)*point.y + H.at<double>(1, 2));

    return cv::Point2d(x, y);
  }

  cv::Mat ImageRotate(const cv::Mat& img, double angle) {
    cv::Mat M = cv::Mat::zeros(cv::Size(3, 2), CV_64FC1);
    M.at<double>(0, 0) = cos(angle);
    M.at<double>(0, 1) = -sin(angle);
    M.at<double>(1, 0) = sin(angle);
    M.at<double>(1, 1) = cos(angle);

    cv::Point2d corners[3];
    corners[0] = ApplyProjection6(cv::Point2d(img.cols, 0), M);
    corners[1] = ApplyProjection6(cv::Point2d(img.cols, img.rows), M);
    corners[2] = ApplyProjection6(cv::Point2d(0, img.rows), M);

    cv::Point2d min_point(0, 0), max_point(0, 0);
    for (int i = 0; i < 3; ++i) {
      min_point.x = std::min(corners[i].x, min_point.x);
      min_point.y = std::min(corners[i].y, min_point.y);
      max_point.x = std::max(corners[i].x, max_point.x);
      max_point.y = std::max(corners[i].y, max_point.y);
    }

    M.at<double>(0, 2) -= min_point.x;
    M.at<double>(1, 2) -= min_point.y;

    cv::Mat dst;
    int nw, nh;
    nw = max_point.x - min_point.x + 1;
    nh = max_point.y - min_point.y + 1;
    cv::warpAffine(img, dst, M, cv::Size(nw, nh));

    return dst;
  }

  cv::Mat ImageWarpAffine(const cv::Mat& image, const cv::Mat& M) {
    cv::Point2d corners[3];
    corners[0] = ApplyProjection6(cv::Point2d(image.cols, 0), M);
    corners[1] = ApplyProjection6(cv::Point2d(image.cols, image.rows), M);
    corners[2] = ApplyProjection6(cv::Point2d(0, image.rows), M);

    cv::Point2d min_point, max_point;
    min_point = max_point = ApplyProjection6(cv::Point2d(0, 0), M);
    for (int i = 0; i < 3; ++i) {
      min_point.x = std::min(min_point.x, corners[i].x);
      min_point.y = std::min(min_point.y, corners[i].y);
      max_point.x = std::max(max_point.x, corners[i].x);
      max_point.y = std::max(max_point.y, corners[i].y);
    }

    cv::Mat M1 = M.clone();
    M1.at<double>(0, 2) -= min_point.x;
    M1.at<double>(1, 2) -= min_point.y;
    int nh, nw;
    nh = max_point.y - min_point.y + 1;
    nw = max_point.x - min_point.x + 1;

    cv::Mat affine_image;
    cv::warpAffine(image, affine_image, M1, cv::Size(nw, nh));

    return affine_image;
  }

  cv::Mat ImageWarpPerspective(const cv::Mat& image, const cv::Mat& H) {
    cv::Point2d corners[3];
    corners[0] = ApplyProjection9(cv::Point2d(image.cols, 0), H);
    corners[1] = ApplyProjection9(cv::Point2d(image.cols, image.rows), H);
    corners[2] = ApplyProjection9(cv::Point2d(0, image.rows), H);

    cv::Point2d min_point, max_point;
    min_point = max_point = ApplyProjection9(cv::Point2d(0, 0), H);
    for (int i = 0; i < 3; ++i) {
      min_point.x = std::min(min_point.x, corners[i].x);
      min_point.y = std::min(min_point.y, corners[i].y);
      max_point.x = std::max(max_point.x, corners[i].x);
      max_point.y = std::max(max_point.y, corners[i].y);
    }

    cv::Mat H1 = H.clone();
    H1.row(0) -= min_point.x*H.row(2);
    H1.row(1) -= min_point.y*H.row(2);
    int nh, nw;
    nh = max_point.y - min_point.y + 1;
    nw = max_point.x - min_point.x + 1;

    cv::Mat affine_image;
    cv::warpPerspective(image, affine_image, H1, cv::Size(nw, nh));

    return affine_image;
  }

  cv::Mat AddAlphaChannel(const cv::Mat& im) {
    std::vector<cv::Mat> channels;
    cv::split(im, channels);
    channels.emplace_back(cv::Mat(im.size(), CV_8UC1, cv::Scalar(255)));

    cv::Mat result;
    cv::merge(channels, result);

    return result;
  }

  cv::Mat AddAlphaChannel(const cv::Mat& im, const cv::Mat& alpha) {
    std::vector<cv::Mat> channels;
    cv::split(im, channels);
    channels.emplace_back(alpha);

    cv::Mat result;
    cv::merge(channels, result);

    return result;
  }

  cv::Mat DelAlphaChannel(const cv::Mat& im) {
    std::vector<cv::Mat> channels;
    cv::split(im, channels);
    channels.resize(channels.size() - 1);

    cv::Mat result;
    cv::merge(channels, result);

    return result;
  }

  cv::Mat GetAlphaChannel(const cv::Mat& im) {
    std::vector<cv::Mat> channels;
    cv::split(im, channels);

    return channels[3];
  }

  double rad(double d) {
    return d * PI / 180.0;
  }

  double deg(double x) {
    return x * 180 / PI;
  }

  void SchmidtOrthogonalization(cv::Mat &mat) {
    int rows = mat.rows;
    int cols = mat.cols;
    for (int col = 1; col < mat.cols; ++col) {
      cv::Mat &current_col = mat.col(col);
      for (int i = 0; i < col; ++i) {
        cv::Mat col_vec = mat.col(i);
        current_col -= col_vec.dot(current_col) / col_vec.dot(col_vec)*col_vec;
      }
    }

    for (int col = 0; col < mat.cols; ++col) {
      cv::Mat &current_col = mat.col(col);
      current_col /= sqrt(current_col.dot(current_col));
    }
  }

  Eigen::Vector2d ComputeThatLonLat(double lon, double lat, double brng, double dist) {
    /*
    * WGS-84 a=6378137 b=6356752.3142 f=1/298.2572236
    */
    double a = 6378137;
    double b = 6356752.3142;
    double f = 1 / 298.2572236;

    double alpha1 = rad(brng);
    double sinAlpha1 = sin(alpha1);
    double cosAlpha1 = cos(alpha1);

    double tanU1 = (1 - f) * tan(rad(lat));
    double cosU1 = 1 / sqrt((1 + tanU1 * tanU1));
    double sinU1 = tanU1 * cosU1;
    double sigma1 = atan2(tanU1, cosAlpha1);
    double sinAlpha = cosU1 * sinAlpha1;
    double cosSqAlpha = 1 - sinAlpha * sinAlpha;
    double uSq = cosSqAlpha * (a * a - b * b) / (b * b);
    double A = 1 + uSq / 16384 * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
    double B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));

    double cos2SigmaM = 0;
    double sinSigma = 0;
    double cosSigma = 0;
    double sigma = dist / (b * A), sigmaP = 2 * PI;
    while (abs(sigma - sigmaP) > 1e-12) {
      cos2SigmaM = cos(2 * sigma1 + sigma);
      sinSigma = sin(sigma);
      cosSigma = cos(sigma);
      double deltaSigma = B * sinSigma * (cos2SigmaM + B / 4 * (cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM)
        - B / 6 * cos2SigmaM * (-3 + 4 * sinSigma * sinSigma) * (-3 + 4 * cos2SigmaM * cos2SigmaM)));
      sigmaP = sigma;
      sigma = dist / (b * A) + deltaSigma;
    }

    double tmp = sinU1 * sinSigma - cosU1 * cosSigma * cosAlpha1;
    double lat2 = atan2(sinU1 * cosSigma + cosU1 * sinSigma * cosAlpha1,
      (1 - f) * sqrt(sinAlpha * sinAlpha + tmp * tmp));
    double lambda = atan2(sinSigma * sinAlpha1, cosU1 * cosSigma - sinU1 * sinSigma * cosAlpha1);
    double C = f / 16 * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha));
    double L = lambda - (1 - C) * f * sinAlpha
      * (sigma + C * sinSigma * (cos2SigmaM + C * cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM)));

    double revAz = atan2(sinAlpha, -tmp); // final bearing

    Eigen::Vector2d target;
    target(1) = deg(lat2);
    target(0) = lon + deg(L);

    return target;
  }
}