#include "ImageStitching.h"
#include "util.h"
#include <pugixml/pugixml.hpp>
#include <timer.h>

//gdal
#include <gdal_priv.h>
#include <cpl_error.h>
#include <ogr_spatialref.h>

namespace AirLookUAV {
  template<typename T>
  void FeatureExtract(const std::shared_ptr<ImageInfo>& image_info, const T& extractor) {
    if (image_info->mask.empty()) {
      extractor->detect(image_info->rotate_image, image_info->key_points);
    }
    else {
      extractor->detect(image_info->rotate_image, image_info->key_points, image_info->mask);
    }
    
    extractor->compute(image_info->rotate_image, image_info->key_points, image_info->descriptors);
  }

  cv::Mat ImageStitching::Stitching(std::vector<std::shared_ptr<ImageInfo>>& image_infos, Point4Pos& point4_pos) {
    cv::Ptr<cv::ORB> orb = cv::ORB::create(15000);
    cv::Ptr<cv::xfeatures2d::SURF> surf = cv::xfeatures2d::SURF::create(300);
    cv::Ptr<cv::xfeatures2d::SIFT> sift = cv::xfeatures2d::SIFT::create(3000);

#define SURF1
    std::unordered_map<int, std::shared_ptr<ImageInfo>> image_infos_map;
    Timer timer;
    timer.Start();
    for (auto& image_info : image_infos) {
#if 1
      Eigen::Quaterniond quat(image_info->quat);
      image_info->R = quat.toRotationMatrix();
      Eigen::Quaterniond R_inv(image_info->quat(0), -image_info->quat(1), -image_info->quat(2), -image_info->quat(3));
      image_info->center = R_inv * -image_info->t;
      image_info->R(2, 0) = 0;
      image_info->R(2, 1) = 0;
      image_info->R(2, 2) = 1;

      Eigen::Matrix3d H = image_info->K*image_info->R*image_info->K.inverse();
      cv::Mat homo = (cv::Mat_<double>(3, 3) << H(0, 0), H(0, 1), H(0, 2), H(1, 0), H(1, 1), H(1, 2), H(2, 0), H(2, 1), H(2, 2));

      cv::Mat img = AddAlphaChannel(image_info->image);
      image_info->rotate_image = ImageWarpAffine(img, homo.rowRange(0, 2));
      image_info->mask = GetAlphaChannel(image_info->rotate_image);
#endif
      image_info->homo = cv::Mat::eye(cv::Size(3, 3), CV_64FC1);

#if defined(ORB1)
      FeatureExtract(image_info, orb);
#elif defined(SURF1)
      FeatureExtract(image_info, surf);
#else
      FeatureExtract(image_info, sift);
#endif
      image_infos_map[image_info->id] = image_info;
    }

    std::cout << "feature extract cost time: " << timer.ElapsedSeconds() << std::endl;
    timer.Restart();

#if defined(ORB1)
    cv::BFMatcher  bf_matcher(cv::NORM_HAMMING, true);
#else
    cv::BFMatcher  bf_matcher(cv::NORM_L2, true);
#endif
    auto FindHomo = [&bf_matcher](const ImageInfo&image_info1, const ImageInfo& image_info2) {
      std::vector<cv::DMatch> matchs;
      bf_matcher.match(image_info1.descriptors, image_info2.descriptors, matchs);
      std::vector<cv::Point2f> points1, points2;
      points1.reserve(matchs.size());
      points2.reserve(matchs.size());
      for (const auto& match : matchs) {
        points1.emplace_back(image_info1.key_points[match.queryIdx].pt);
        points2.emplace_back(image_info2.key_points[match.trainIdx].pt);
      }

      std::vector<uchar> inlier_mask;
      cv::Mat H = cv::findHomography(points2, points1, cv::RANSAC, 8.0, inlier_mask);

#if 0
      int match_num = matchs.size();
      for (int j = 0; j < inlier_mask.size(); ++j) {
        if (!inlier_mask[j]) {
          inlier_mask.erase(inlier_mask.cbegin() + j);
          matchs.erase(matchs.cbegin() + j);
          --j;
        }
      }

      int inlier_match_num = inlier_mask.size();
      std::cout << "match_num: " << match_num << "\tinlier_match_num: " << inlier_match_num << "\t scale: " << float(inlier_match_num) / match_num << std::endl;

      cv::Mat match;
      cv::Mat im1, im2;
      if (image_info1.image.channels() == 4) {
        im1 = DelAlphaChannel(image_info1.image);
        im2 = DelAlphaChannel(image_info2.image);
      }
      else {
        im1 = image_info1.image;
        im2 = image_info2.image;
      }
      cv::namedWindow("match", 0);
      cv::drawMatches(im1, image_info1.key_points, im2, image_info2.key_points, matchs, match);
      cv::imshow("match", match);
      cv::waitKey(0);
#endif

      return H;
    };
    std::map<int, Eigen::Vector3d> prev_strip_centers;
    std::map<int, Eigen::Vector3d> current_strip_centers;

    auto GetNearestImageId = [&prev_strip_centers](const Eigen::Vector3d& center) {
      int result_id;
      double distance = std::numeric_limits<double>::max();
      for (auto& strip_center: prev_strip_centers) {
        double d = (center - strip_center.second).head(2).norm();
        if (distance > d) {
          result_id = strip_center.first;
          distance = d;
        }
      }

      return result_id;
    };
    current_strip_centers[image_infos[0]->id] = image_infos[0]->center;
    int strip_id = 0;
    for (int i = 1; i < image_infos.size(); ++i) {
      int prev_id = -1;
      if (image_infos[i]->air_strip == 0) {
        prev_id = image_infos[i - 1]->id;
      }
      else {
        if (strip_id != image_infos[i]->air_strip) {
          strip_id = image_infos[i]->air_strip;
          prev_strip_centers = current_strip_centers;
          current_strip_centers.clear();
        }

        prev_id = GetNearestImageId(image_infos[i]->center);
      }

      current_strip_centers[image_infos[i]->id] = image_infos[i]->center;

      cv::Mat H = FindHomo(*(image_infos_map[prev_id].get()), *(image_infos[i].get()));
      image_infos[i]->homo = image_infos_map[prev_id]->homo*H;
    }

    std::cout << "feature match cost time: " << timer.ElapsedSeconds() << std::endl;
    timer.Restart();
    cv::Mat image = Bleeding(image_infos, point4_pos);
    std::cout << "Bleeding cost time: " << timer.ElapsedSeconds() << std::endl;

   /* if (!tiff_path.empty()) {
      GenerateTiff(image, point4_pos, tiff_path);
    }*/
      
    return image;
  }

  template<typename T>
  void LineOf2Points(T &a, T &b, T &c, T x1, T y1, T x2, T y2) {
    if (fabs(x1 - x2)<0.000001) {
      a = 1.0f;
      b = 0;
      c = -x1;
    }
    else {
      a = (y1 - y2) / (x1 - x2);
      b = -1.0f;
      c = y1 - a*x1;
    }
  }

  template <typename T, typename E>
  T FindMinDist(T* a, T* b, T* c, T* inv_a2_plus_b2, int line_num, E x, E y) {
    T min_dist;
    for (int i = 0; i < line_num; ++i) {
      T dist = abs(a[i] * x + b[i] * y + c[i])*inv_a2_plus_b2[i];
      if (i < 1) {
        min_dist = dist;
      }
      else {
        min_dist = std::min(min_dist, dist);
      }
    }

    return min_dist;
  }

  Eigen::Matrix<double, 6, 1> GetAffineTransform(const std::vector<Eigen::Vector2d>& src, const std::vector<Eigen::Vector2d>& dst) {
    Eigen::Matrix<double, Eigen::Dynamic, 6> A(src.size()*2, 6);
    Eigen::Matrix<double, Eigen::Dynamic, 1> B(src.size() * 2, 1);

    for (int i = 0; i < src.size(); ++i) {
      int idx = i << 1;
      A.row(idx) << src[i](0), src[i](1), 1, 0, 0, 0;
      A.row(idx + 1) << 0, 0, 0, src[i](0), src[i](1), 1;
      B(idx, 0) = dst[i](0);
      B(idx + 1, 0) = dst[i](1);
    }

    auto ATA = A.transpose()*A;
    auto ATB = A.transpose()*B;

    auto X = ATA.inverse()*ATB;

    return X;
  }

  void CalculateDistMaps(std::vector<ImageStitchingInfo>& image_stitching_infos, std::vector<float*>& dist_maps) {
    dist_maps.clear();

    for (auto& image_stitching_info : image_stitching_infos) {
      double a[4], b[4], c[4], inv_a2_plus_b2[4];
      cv::Point2d* corners = image_stitching_info.corners;

      for (int i = 0; i < 4; ++i) {
        LineOf2Points(a[i], b[i], c[i], corners[i].x, corners[i].y, corners[(i + 1) % 4].x, corners[(i + 1) % 4].y);
        inv_a2_plus_b2[i] = 1 / sqrt(a[i] * a[i] + b[i] * b[i]);
      }

      cv::Mat& mask = image_stitching_info.mask;

      int size = mask.cols*mask.rows;
      float *dist_map = new float[size];
      memset(dist_map, 0, sizeof(float)*size);
      uchar* data = mask.data;
      float max_dist = 0;

      for (int h = 0; h < mask.rows; ++h) {
        float* row_dist_map = dist_map + h*mask.cols;
        int step0 = mask.step[0];
        uchar* row_data = data + h*mask.step[0];
        for (int w = 0; w < mask.cols; ++w) {
          if (row_data[w] == 0) {
            continue;
          }

          row_dist_map[w] = FindMinDist(a, b, c, inv_a2_plus_b2, 4, w, h);
          max_dist = std::max(max_dist, row_dist_map[w]);
        }
      }

      //normalize dist_map
      for (int idx = 0; idx < size; ++idx) {
        dist_map[idx] /= max_dist;
      }

      dist_maps.emplace_back(dist_map);
      image_stitching_info.bleeding_mask = cv::Mat::zeros(mask.size(), mask.type());
    }
  }

  void UpdateMask(std::vector<ImageStitchingInfo>& image_stitching_infos, int cols, int rows) {
    std::vector<float*> dist_maps;
    CalculateDistMaps(image_stitching_infos, dist_maps);

    auto FindMaxId = [&dist_maps, &image_stitching_infos](int x, int y) {
      int id = -1;
      float dist = 0;

      int x1 = -1, y1 = -1;
      for (int i = 0; i < image_stitching_infos.size(); ++i) {
        const cv::Rect& rect = image_stitching_infos[i].rect;
        if (x > rect.x && x< rect.x + rect.width && y>rect.y && y < rect.y + rect.height) {
          cv::Point point = cv::Point(x, y) - cv::Point(rect.x, rect.y);
          float distance = dist_maps[i][point.y*rect.width + point.x];
          if (dist_maps[i][point.y*rect.width + point.x] > dist) {
            dist = dist_maps[i][point.y*rect.width + point.x];
            id = i;
          }
        }
      }

      return id;
    };

    //update mask
    for (int h = 0; h < rows; ++h) {
      for (int w = 0; w < cols; ++w) {
        int max_id = FindMaxId(w, h);
        if (max_id < 0) {
          continue;
        }

        const cv::Rect& rect = image_stitching_infos[max_id].rect;
        image_stitching_infos[max_id].bleeding_mask.at<uchar>(h - rect.y, w - rect.x) = 255;
      }
    }

  }

  ImageStitching::ImageStitching() {
    orb_ = cv::ORB::create(15000);
    surf_ = cv::xfeatures2d::SURF::create(300);
    sift_ = cv::xfeatures2d::SIFT::create(3000);
    global_min_point_ = cv::Point2d(0, 0);
    global_max_point_ = cv::Point2d(0, 0);

    std::cout << "create ImageStitching*************************************************" << std::endl;
  }

  cv::Mat ImageStitching::FindHomo(const ImageInfo&image_info1, const ImageInfo& image_info2, bool& flag) {
    std::vector<cv::DMatch> matchs;
    
    bf_matcher_->match(image_info1.descriptors, image_info2.descriptors, matchs);
    if (matchs.size() < 100) {
      flag = false;
      return cv::Mat::eye(cv::Size(3, 3), CV_64FC1);
    }
    std::vector<cv::Point2f> points1, points2;
    points1.reserve(matchs.size());
    points2.reserve(matchs.size());
    for (const auto& match : matchs) {
      points1.emplace_back(image_info1.key_points[match.queryIdx].pt);
      points2.emplace_back(image_info2.key_points[match.trainIdx].pt);
    }

    std::vector<uchar> inlier_mask;
    cv::Mat H = cv::findHomography(points2, points1, cv::RANSAC, 8.0, inlier_mask);

    int match_num = matchs.size();
    for (int j = 0; j < inlier_mask.size(); ++j) {
      if (!inlier_mask[j]) {
        inlier_mask.erase(inlier_mask.cbegin() + j);
        matchs.erase(matchs.cbegin() + j);
        --j;
      }
    }

#if 0
    int inlier_match_num = inlier_mask.size();
    std::cout << "match_num: " << match_num << "\tinlier_match_num: " << inlier_match_num << "\t scale: " << float(inlier_match_num) / match_num << std::endl;

    cv::Mat match;
    cv::Mat im1, im2;
    if (image_info1.image.channels() == 4) {
      im1 = DelAlphaChannel(image_info1.image);
      im2 = DelAlphaChannel(image_info2.image);
    }
    else {
      im1 = image_info1.image;
      im2 = image_info2.image;
    }
    cv::namedWindow("match", 0);
    cv::drawMatches(im1, image_info1.key_points, im2, image_info2.key_points, matchs, match);
    cv::imshow("match", match);
    cv::waitKey(0);
#endif

    if (inlier_mask.size() < 30) {
      flag = false;
    }
    return H;
  }

  int ImageStitching::GetNearestImageId(const Eigen::Vector3d& center) {
    int result_id;
    double distance = std::numeric_limits<double>::max();
    for (auto& strip_center : prev_strip_centers_) {
      double d = (center - strip_center.second).head(2).norm();
      if (distance > d) {
        result_id = strip_center.first;
        distance = d;
      }
    }

    return result_id;
  }

  cv::Mat ImageStitching::Bleeding(const std::vector<std::shared_ptr<ImageInfo>>& image_infos, Point4Pos& point4_pos) {
    std::vector<ImageStitchingInfo> image_stitching_infos;
    cv::Point2d global_min_point(0, 0), global_max_point(0, 0);

    cv::Mat kernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3));
    for (auto& image_info : image_infos) {
      cv::Mat im;
      if (image_info->rotate_image.channels() < 4) {
        im = AddAlphaChannel(image_info->rotate_image);
      }
      else {
        im = image_info->rotate_image;
      }

      cv::Mat H = image_info->homo.clone();
      
      ImageStitchingInfo image_stitching_info;
      image_stitching_info.corners[0] = ApplyProjection9(cv::Point2d(0, 0), H);
      image_stitching_info.corners[1] = ApplyProjection9(cv::Point2d(image_info->rotate_image.cols, 0), H);
      image_stitching_info.corners[2] = ApplyProjection9(cv::Point2d(image_info->rotate_image.cols, image_info->rotate_image.rows), H);
      image_stitching_info.corners[3] = ApplyProjection9(cv::Point2d(0, image_info->rotate_image.rows), H);

      cv::Point2d max_point, min_point;

      max_point = min_point = image_stitching_info.corners[0];
      for (int i = 1; i < 4; ++i) {
        min_point.x = std::min(image_stitching_info.corners[i].x, min_point.x);
        min_point.y = std::min(image_stitching_info.corners[i].y, min_point.y);
        max_point.x = std::max(image_stitching_info.corners[i].x, max_point.x);
        max_point.y = std::max(image_stitching_info.corners[i].y, max_point.y);
      }

      global_min_point.x = std::min(min_point.x, global_min_point.x);
      global_min_point.y = std::min(min_point.y, global_min_point.y);
      global_max_point.x = std::max(max_point.x, global_max_point.x);
      global_max_point.y = std::max(max_point.y, global_max_point.y);


      for (int i = 0; i < 4; ++i) {
        image_stitching_info.corners[i] -= min_point;
      }

      image_stitching_info.min_point = min_point;
      H.row(0) -= min_point.x*H.row(2);
      H.row(1) -= min_point.y*H.row(2);

      int nw, nh;
      nw = max_point.x - min_point.x + 1;
      nh = max_point.y - min_point.y + 1;
      cv::warpPerspective(im, image_stitching_info.im, H, cv::Size(nw, nh));
      /*char image_save_path[512];
      sprintf_s(image_save_path, "D:\\image_stitching_result\\tmp\\%08d.png", image_info->id);
      cv::imwrite(image_save_path, image_stitching_info.im);*/
      if (!image_info->mask.empty()) {
        cv::warpPerspective(image_info->mask, image_stitching_info.mask, H, cv::Size(nw, nh));
        cv::threshold(image_stitching_info.mask, image_stitching_info.mask, 128, 255, cv::THRESH_BINARY);
      }
      else {
        cv::Mat alpha_channel = GetAlphaChannel(image_stitching_info.im);
        cv::threshold(alpha_channel, image_stitching_info.mask, 128, 255, cv::THRESH_BINARY);
      }

      image_stitching_infos.emplace_back(image_stitching_info);
    }

    for (int i = 0; i < image_stitching_infos.size(); ++i) {
      cv::Point tmp_point = image_stitching_infos[i].min_point - global_min_point;
      image_stitching_infos[i].rect = cv::Rect(tmp_point.x, tmp_point.y,
        image_stitching_infos[i].im.cols, image_stitching_infos[i].im.rows);
    }

    cv::Point global_size = global_max_point - global_min_point + cv::Point2d(2, 2);

#if 1
    cv::Mat result = cv::Mat(global_size.y, global_size.x, CV_8UC4, cv::Scalar(0, 0, 0, 0));
    for (const auto& image_stitching_info : image_stitching_infos) {
      image_stitching_info.im.copyTo(result(image_stitching_info.rect), image_stitching_info.mask);
    }
    
#else 
    UpdateMask(image_stitching_infos, global_size.x, global_size.y);
    cv::detail::MultiBandBlender blender(false, 5);

    blender.prepare(cv::Rect(0, 0, global_size.x, global_size.y));

    for (const auto& image_stitching_info : image_stitching_infos) {
      std::vector<cv::Mat> channels;
      cv::split(image_stitching_info.im, channels);
      cv::Mat img;
      channels.resize(3);
      cv::merge(channels, img);
      img.convertTo(img, CV_16S);
      blender.feed(img, image_stitching_info.mask, image_stitching_info.min_point - global_min_point);
    }

    cv::Mat result_image, result_mask;
    blender.blend(result_image, result_mask);
    cv::Mat result, mask;
    result_image.convertTo(result, CV_8U);
    result_mask.convertTo(mask, CV_8U);
    result = AddAlphaChannel(result, mask);
#endif

#if 0
    cv::Rect &rect = image_stitching_infos[0].rect;
    Eigen::Vector2d point(rect.x + rect.width / 2, rect.y + rect.height / 2);
    Eigen::Vector3d pos = image_infos[0]->pos;
    std::pair<Eigen::Vector2d, Eigen::Vector3d> point2pos;
    point2pos.first = point;
    point2pos.second = pos;
    CalcuPoint4Pos(result.size(), image_infos[0]->K, point2pos, point4_pos);
#endif
    return result;
}

  void ImageStitching::LoadImageInfoFromXml(const std::string& xml_path, std::vector<std::shared_ptr<ImageInfo>>& image_infos) {
  pugi::xml_document doc;
  pugi::xml_parse_result result = doc.load_file(xml_path.c_str());
  pugi::xml_node block = doc.child("BlocksExchange").child("Block");

  pugi::xml_node photo_groups = block.child("Photogroups");
  for (pugi::xml_node photo_group = photo_groups.first_child(); photo_group != NULL; photo_group = photo_group.next_sibling()) {
    double focal_length = photo_group.child("FocalLengthPixels").text().as_double();
    double cx = photo_group.child("PrincipalPoint").child("x").text().as_double();
    double cy = photo_group.child("PrincipalPoint").child("y").text().as_double();
    double k1 = photo_group.child("Distortion").child("K1").text().as_double();
    double k2 = photo_group.child("Distortion").child("K2").text().as_double();
    double k3 = photo_group.child("Distortion").child("K3").text().as_double();
    double p1 = photo_group.child("Distortion").child("P1").text().as_double();
    double p2 = photo_group.child("Distortion").child("P2").text().as_double();

    std::vector<double> dist_coeffs({ k1, k2, p1, p2, k3 });

    Eigen::Matrix3d K = Eigen::Matrix3d::Zero();
#if 1
    K(0, 0) = K(1, 1) = focal_length;
    K(0, 2) = cx;
    K(1, 2) = cy;
    K(2, 2) = 1;
#elif 1
    K.at<double>(0, 0) = K.at<double>(1, 1) = 3666.666504;
    K.at<double>(0, 2) = 2736;
    K.at<double>(1, 2) = 1824;
    K.at<double>(2, 2) = 1;
#else 
    K.at<double>(0, 0) = K.at<double>(1, 1) = 723.553;
    K.at<double>(0, 2) = 480;
    K.at<double>(1, 2) = 360;
    K.at<double>(2, 2) = 1;
#endif
    std::unordered_map<int, int> image_to_air_strip;

    std::vector<std::pair<int, int>> air_strips;
    air_strips.emplace_back(0, 20);
    air_strips.emplace_back(23, 41);
    air_strips.emplace_back(44, 62);
    air_strips.emplace_back(65, 83);
    air_strips.emplace_back(86, 104);
    air_strips.emplace_back(107, 126);
    air_strips.emplace_back(129, 148);
    air_strips.emplace_back(151, 170);
    air_strips.emplace_back(173, 193);

    for (int i = 0; i < air_strips.size(); ++i) {
      for (int j = air_strips[i].first; j <= air_strips[i].second; ++j) {
        image_to_air_strip[j] = i;
      }
    }
    for (auto& photo : photo_group.children("Photo")) {
      std::shared_ptr<ImageInfo> image_info = std::make_shared<ImageInfo>();
      image_info->K = K;

      image_info->id = photo.child("Id").text().as_int();
      std::string image_path = photo.child("ImagePath").text().as_string();
      image_info->image = cv::imread(image_path, CV_LOAD_IMAGE_UNCHANGED);
      //drone - dji:GpsLatitude = "39.55684310"
        //drone - dji : GpsLongtitude = "117.40186308"
      image_info->pos(0) = photo.child("Pose").child("Metadata").child("Center").child("x").text().as_double();
      image_info->pos(1) = photo.child("Pose").child("Metadata").child("Center").child("y").text().as_double();
      image_info->pos(2) = photo.child("Pose").child("Metadata").child("Center").child("z").text().as_double();
      if (!image_to_air_strip.count(image_info->id)) {
        continue;
      }
      image_info->air_strip = image_to_air_strip[image_info->id];
      for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
          image_info->R(i, j) = photo.child("Pose").child("Rotation").child((std::string("M_")
            + std::to_string(i) + std::to_string(j)).c_str()).text().as_double();
        }
      }

      image_info->center(0) = photo.child("Pose").child("Center").child("x").text().as_double();
      image_info->center(1) = photo.child("Pose").child("Center").child("y").text().as_double();
      image_info->center(2) = photo.child("Pose").child("Center").child("z").text().as_double();

      std::cout << "load image: " << image_path << std::endl;

      image_infos.emplace_back(image_info);
    }
  }
}

  void ImageStitching::CalcuPoint4Pos(const cv::Size& image_size, const Eigen::Matrix3d& K,
    const std::pair<Eigen::Vector2d, Eigen::Vector3d>& point2pos, Point4Pos& point4_pos) {
    double realWidth, realHeight;
    double left_height, left_width, right_height, right_width;

    left_height = point2pos.second(2) * point2pos.first(1) / K(0, 0);
    left_width = point2pos.second(2) * point2pos.first(0) / K(0, 0);
    right_height = point2pos.second(2)*(image_size.height - point2pos.first(1)) / K(0, 0);
    right_width = point2pos.second(2)*(image_size.width - point2pos.first(0)) / K(0, 0);
    double rng1 = deg(atan(left_height/left_width) + PI);
    int dist1 = sqrt(left_height*left_height + left_width*left_width);
    point4_pos.left_top = ComputeThatLonLat(point2pos.second(0), point2pos.second(1), rng1, dist1);
    double rng2 = deg(2 * PI - atan(right_height / right_width));
    int dist2 = sqrt(right_height*right_height + right_height*right_height);
    point4_pos.right_bottom = ComputeThatLonLat(point2pos.second(0), point2pos.second(1), rng2, dist2);
   
    point4_pos.right_top = Eigen::Vector2d(point4_pos.right_bottom(0), point4_pos.left_top(1));
    point4_pos.left_bottom = Eigen::Vector2d(point4_pos.left_top(0), point4_pos.right_bottom(1));
  }

  void ImageStitching::UpdateHomo(std::shared_ptr<ImageInfo>& image_info) {
    int prev_id = -1;

    if (image_info->air_strip == 0) {
      prev_id = image_infos_.back()->id;
    }
    else {
      if (strip_id_ != image_info->air_strip) {
        strip_id_ = image_info->air_strip;
        prev_strip_centers_ = current_strip_centers_;
        current_strip_centers_.clear();
      }

      prev_id = GetNearestImageId(image_info->center);
    }

    current_strip_centers_[image_info->id] = image_info->center;

    bool flag = true;
    cv::Mat homo = FindHomo(*(image_infos_map_[prev_id].get()), *(image_info.get()), flag);
    if (!flag) {
      prev_id = image_infos_.back()->id;
      homo = FindHomo(*(image_infos_map_[prev_id].get()), *(image_info.get()), flag);
    }

#if 1
    double f = image_info->K(0, 0);
    double h = image_info->pos(2);
    double scale = f / h;
    double x = (image_info->center(0) - image_infos_map_[prev_id]->center(0))*scale;
    double y = (image_info->center(1) - image_infos_map_[prev_id]->center(1))*scale;
    homo = cv::Mat::eye(cv::Size(3, 3), CV_64FC1);
    homo.at<double>(0, 2) = x;
    homo.at<double>(1, 2) = y;
#endif

    std::cout << "prev_id: " << prev_id << " , id: " << image_info->id << std::endl;
    std::cout << "H: " << homo << std::endl;

#if 0
    homo(cv::Range(0, 2), cv::Range(0, 2)) = cv::Mat::eye(2, 2, CV_64FC1);
    homo.at<double>(2, 0) = 0;
    homo.at<double>(2, 1) = 0;
    homo.at<double>(2, 2) = 1;
#elif 0
    SchmidtOrthogonalization(homo(cv::Range(0, 2), cv::Range(0, 2)));
    homo.at<double>(2, 0) = 0;
    homo.at<double>(2, 1) = 0;
    homo.at<double>(2, 2) = 1;
#endif
    image_info->homo = image_infos_map_[prev_id]->homo*homo;
  }

  cv::Mat ImageStitching::Bleeding() {
    cv::Point global_size = global_max_point_ - global_min_point_ + cv::Point2d(2, 2);

    for (int i = 0; i < image_stitching_infos_.size(); ++i) {
      cv::Point tmp_point = image_stitching_infos_[i].min_point - global_min_point_;
      image_stitching_infos_[i].rect = cv::Rect(tmp_point.x, tmp_point.y,
        image_stitching_infos_[i].im.cols, image_stitching_infos_[i].im.rows);
    }

#if 1
    cv::Mat result = cv::Mat(global_size.y, global_size.x, CV_8UC4, cv::Scalar(0, 0, 0, 0));
    for (const auto& image_stitching_info : image_stitching_infos_) {
      image_stitching_info.im.copyTo(result(image_stitching_info.rect), image_stitching_info.mask);
    }
#else 
    UpdateMask(image_stitching_infos_, global_size.x, global_size.y);
    cv::detail::MultiBandBlender blender(false, 5);

    blender.prepare(cv::Rect(0, 0, global_size.x, global_size.y));

    for (const auto& image_stitching_info : image_stitching_infos_) {
      std::vector<cv::Mat> channels;
      cv::split(image_stitching_info.im, channels);
      cv::Mat img;
      channels.resize(3);
      cv::merge(channels, img);
      img.convertTo(img, CV_16S);
      blender.feed(img, image_stitching_info.bleeding_mask, image_stitching_info.min_point - global_min_point_);
  }

    cv::Mat result_image, result_mask;
    blender.blend(result_image, result_mask);
    cv::Mat result, mask;
    result_image.convertTo(result, CV_8U);
    result_mask.convertTo(mask, CV_8U);
    result = AddAlphaChannel(result, mask);
#endif

    return result;
  }

  cv::Mat ImageStitching::Stitching(std::shared_ptr<ImageInfo>& image_info, Point4Pos& point4_pos) {
    AddImageInfo(image_info);
    
    cv::Mat result = Bleeding(point4_pos);

    std::cout << "add image: " << image_info->id << std::endl;
    return result;
  }

  bool ImageStitching::AddImageInfo(std::shared_ptr<ImageInfo>& image_info) {
    std::cout << "enter*****************Stitching************************" << std::endl;
#if 1
    /*Eigen::Quaterniond quat(image_info->quat);
    image_info->R = quat.toRotationMatrix();
    Eigen::Quaterniond R_inv(image_info->quat(0), -image_info->quat(1), -image_info->quat(2), -image_info->quat(3));
    image_info->center = R_inv * -image_info->t;*/
    image_info->R(2, 0) = 0;
    image_info->R(2, 1) = 0;
    image_info->R(2, 2) = 1;

    Eigen::Matrix3d H = image_info->K*image_info->R*image_info->K.inverse();
    cv::Mat homo = (cv::Mat_<double>(3, 3) << H(0, 0), H(0, 1), H(0, 2), H(1, 0), H(1, 1), H(1, 2), H(2, 0), H(2, 1), H(2, 2));

    cv::Mat img = AddAlphaChannel(image_info->image);
    image_info->rotate_image = ImageWarpAffine(img, homo.rowRange(0, 2));
    cv::flip(image_info->rotate_image, image_info->rotate_image, 0);
    image_info->mask = GetAlphaChannel(image_info->rotate_image);
#else
    image_info->rotate_image = image_info->image;
#endif
    image_info->homo = cv::Mat::eye(cv::Size(3, 3), CV_64FC1);

#if 1
#define SURF1
#if defined(ORB1)
    FeatureExtract(image_info, orb_);
#elif defined(SURF1)
    FeatureExtract(image_info, surf_);
#else
    FeatureExtract(image_info, sift_);
#endif

#if defined(ORB1)
    bf_matcher_ = cv::BFMatcher::create(cv::NORM_HAMMING, true);
#else
    bf_matcher_ = cv::BFMatcher::create(cv::NORM_L2, true);
#endif

    image_infos_map_[image_info->id] = image_info;
    if (image_infos_.empty()) {
      image_infos_.emplace_back(image_info);
      ImageStitchingInfo image_stitching_info;
      GenerateImageStitchingInfo(image_info, image_stitching_info);
      image_stitching_info.id = image_info->id;
      image_stitching_infos_.emplace_back(image_stitching_info);
      return true;
    }

    UpdateHomo(image_info);
    std::cout << "id: " << image_info->id << std::endl << "H: " << image_info->homo << std::endl;
    image_infos_.emplace_back(image_info);
    ImageStitchingInfo image_stitching_info;
    GenerateImageStitchingInfo(image_info, image_stitching_info);
    image_stitching_info.id = image_info->id;
    image_stitching_infos_.emplace_back(image_stitching_info);
#endif


    return true;
  }

  cv::Mat ImageStitching::Bleeding(Point4Pos& point4_pos) {
    cv::Mat result;
    if (image_stitching_infos_.size() < 2) {
      result = image_stitching_infos_[0].im;
    }
    else {
      result = Bleeding();
    }
    
    CalculatePoint4Pos(result.size(), point4_pos);

   /* if (!M1_.empty()) {
      std::cout << "M1: " << M1_ << std::endl;
      result = ImageWarpAffine(result, M1_);
    }*/

    return result;
  }

  void ImageStitching::GenerateImageStitchingInfo(const std::shared_ptr<ImageInfo>& image_info, ImageStitchingInfo& image_stitching_info) {
    cv::Mat im;
    if (image_info->rotate_image.channels() < 4) {
      im = AddAlphaChannel(image_info->rotate_image);
    }
    else {
      im = image_info->rotate_image;
    }

    cv::Mat H = image_info->homo.clone();

    image_stitching_info.corners[0] = ApplyProjection9(cv::Point2d(0, 0), H);
    image_stitching_info.corners[1] = ApplyProjection9(cv::Point2d(image_info->rotate_image.cols, 0), H);
    image_stitching_info.corners[2] = ApplyProjection9(cv::Point2d(image_info->rotate_image.cols, image_info->rotate_image.rows), H);
    image_stitching_info.corners[3] = ApplyProjection9(cv::Point2d(0, image_info->rotate_image.rows), H);

    cv::Point2d max_point, min_point;

    max_point = min_point = image_stitching_info.corners[0];
    for (int i = 1; i < 4; ++i) {
      min_point.x = std::min(image_stitching_info.corners[i].x, min_point.x);
      min_point.y = std::min(image_stitching_info.corners[i].y, min_point.y);
      max_point.x = std::max(image_stitching_info.corners[i].x, max_point.x);
      max_point.y = std::max(image_stitching_info.corners[i].y, max_point.y);
    }

    global_min_point_.x = std::min(min_point.x, global_min_point_.x);
    global_min_point_.y = std::min(min_point.y, global_min_point_.y);
    global_max_point_.x = std::max(max_point.x, global_max_point_.x);
    global_max_point_.y = std::max(max_point.y, global_max_point_.y);


    for (int i = 0; i < 4; ++i) {
      image_stitching_info.corners[i] -= min_point;
    }

    image_stitching_info.min_point = min_point;
    H.row(0) -= min_point.x*H.row(2);
    H.row(1) -= min_point.y*H.row(2);

    int nw, nh;
    nw = max_point.x - min_point.x + 1;
    nh = max_point.y - min_point.y + 1;
    cv::warpPerspective(im, image_stitching_info.im, H, cv::Size(nw, nh));
    /*char image_save_path[512];
    sprintf_s(image_save_path, "D:\\image_stitching_result\\tmp\\%08d.png", image_info->id);
    cv::imwrite(image_save_path, image_stitching_info.im);*/
    if (!image_info->mask.empty()) {
      cv::warpPerspective(image_info->mask, image_stitching_info.mask, H, cv::Size(nw, nh));
      cv::threshold(image_stitching_info.mask, image_stitching_info.mask, 128, 255, cv::THRESH_BINARY);
    }
    else {
      cv::Mat alpha_channel = GetAlphaChannel(image_stitching_info.im);
      cv::threshold(alpha_channel, image_stitching_info.mask, 128, 255, cv::THRESH_BINARY);
    }

    image_stitching_info.pos = image_info->pos;
  }

  void ImageStitching::CalculatePoint4Pos(const cv::Size& image_size, Point4Pos& point4_pos) {
    std::vector<std::pair<Eigen::Vector2d, Eigen::Vector3d>> point2pos;
    for (auto& image_stitching_info : image_stitching_infos_) {
      cv::Rect &rect = image_stitching_info.rect;
      Eigen::Vector2d point(rect.x + rect.width / 2, rect.y + rect.height / 2);
      Eigen::Vector3d pos = image_stitching_info.pos;
      point2pos.emplace_back(point, pos);
    }

#if 1
    if (image_stitching_infos_.size() > 2) {
      std::vector<Eigen::Vector2d> src;
      std::vector<Eigen::Vector2d> dst;
      for (auto& tmp : point2pos) {
        src.push_back(tmp.first);
        dst.push_back(Eigen::Vector2d(tmp.second(0), tmp.second(1)));
      }
      
      Eigen::Matrix<double, 6, 1> M = GetAffineTransform(src, dst);

      std::cout << "affine M: " << M << std::endl;
      M_ = (cv::Mat_<double>(2, 3) << M(0, 0), M(1, 0), M(2, 0), M(3, 0), M(4, 0), M(5, 0));
      //M_ = (cv::Mat_<double>(2, 3) << M(0, 0), 0, M(2, 0), 0, M(4, 0), M(5, 0));
      //M1_ = (cv::Mat_<double>(2, 3) << 1, M(1, 0)/M(0, 0), M(2, 0), M(4, 0)/M(3, 0), 1, M(5, 0));
      cv::Point2d point = ApplyProjection6(cv::Point2d(0, 0), M_);
      point4_pos.left_top = Eigen::Vector2d(point.x, point.y);
      point = ApplyProjection6(cv::Point2d(image_size.width, 0), M_);
      point4_pos.right_top = Eigen::Vector2d(point.x, point.y);
      point = ApplyProjection6(cv::Point2d(image_size.width, image_size.height), M_);
      point4_pos.right_bottom = Eigen::Vector2d(point.x, point.y);
      point = ApplyProjection6(cv::Point2d(0, image_size.height), M_);
      point4_pos.left_bottom = Eigen::Vector2d(point.x, point.y);   

      return;
    }   
#endif
    CalcuPoint4Pos(image_size, image_infos_[0]->K, point2pos[0], point4_pos);
  }

  void ImageStitching::GenerateTiff(const std::string& png_path, const Point4Pos& point4_pos, const std::string& tiff_path) {
    GDALAllRegister();
    OGRRegisterAll();
    GDALDataset* src = (GDALDataset *)GDALOpenEx(png_path.c_str(), GDAL_OF_RASTER, NULL, NULL, NULL);
    if (src == NULL) {
      return;
    }

    GDALDriver *poDriver = GetGDALDriverManager()->GetDriverByName("GTiff");

    // Create the spatial reference system for the file
    OGRSpatialReference oSRS;
    if (oSRS.importFromEPSG(4326) != OGRERR_NONE) {

    }
    char *pszDstWKT = NULL;
    if (oSRS.exportToWkt(&pszDstWKT) != OGRERR_NONE) {
      CPLFree(pszDstWKT);
    }
    // Create the GTiff file
    GDALDataset *dst = poDriver->CreateCopy(tiff_path.c_str(), src, FALSE, NULL, NULL, NULL);

    // Set the projection
    if (dst->SetProjection(pszDstWKT) != CE_None) {
      dst->Release();
      CPLFree(pszDstWKT);
    }
    CPLFree(pszDstWKT);

    //���Ͻǵ�
    double dLon = point4_pos.left_top(0);
    double dEndlat = point4_pos.left_top(1);
    //d
    double dEndLon = point4_pos.right_bottom(0);
    double dLat = point4_pos.right_bottom(1);

    double resolutionX = (dEndLon - dLon) / src->GetRasterXSize();
    double resolutionY = (dEndlat - dLat) / dst->GetRasterYSize();
    double adfGeoTransform[6] = { dLon, resolutionX , 0, dEndlat, 0, -resolutionY };
    // Apply the geo transform
    if (dst->SetGeoTransform(adfGeoTransform) != CE_None) {
      dst->Release();
    }

    GDALClose((GDALDatasetH)dst);
    GDALClose((GDALDatasetH)src);
  }

  void ImageStitching::GenerateTiff(const std::string& png_path, const std::string& tiff_path) {
    GDALAllRegister();
    OGRRegisterAll();
    GDALDataset* src = (GDALDataset *)GDALOpenEx(png_path.c_str(), GDAL_OF_RASTER, NULL, NULL, NULL);
    if (src == NULL) {
      return;
    }

    GDALDriver *poDriver = GetGDALDriverManager()->GetDriverByName("GTiff");

    // Create the spatial reference system for the file
    OGRSpatialReference oSRS;
    if (oSRS.importFromEPSG(4326) != OGRERR_NONE) {

    }
    char *pszDstWKT = NULL;
    if (oSRS.exportToWkt(&pszDstWKT) != OGRERR_NONE) {
      CPLFree(pszDstWKT);
    }

    char *option1 = "TILED=YES";
    char *option2 = "COMPRESS=PACKBITS";
    char *papszOptions[2];
    papszOptions[0] = option1;
    papszOptions[1] = option2;
    // Create the GTiff file
    GDALDataset *dst = poDriver->CreateCopy(tiff_path.c_str(), src, FALSE, papszOptions, NULL, NULL);

    // Set the projection
    if (dst->SetProjection(pszDstWKT) != CE_None) {
      dst->Release();
      CPLFree(pszDstWKT);
    }
    CPLFree(pszDstWKT);

    double transform[6];
    transform[0] = M_.at<double>(0, 2);
    transform[1] = M_.at<double>(0, 0);
    transform[2] = M_.at<double>(0, 1);
    transform[3] = M_.at<double>(1, 2);
    transform[4] = M_.at<double>(1, 0);
    transform[5] = M_.at<double>(1, 1);
    
    // Apply the geo transform
    if (dst->SetGeoTransform(transform) != CE_None) {
      dst->Release();
    }

    GDALClose((GDALDatasetH)dst);
    GDALClose((GDALDatasetH)src);
  }
}