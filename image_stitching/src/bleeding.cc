#include "bleeding.h"
#include <pugixml/pugixml.hpp>
#include "Eigen/Eigen"

namespace AirlookStitching {
  inline cv::Point2d ApplyProjection9(const cv::Point2d& point, const cv::Mat& H) {
    double x = (H.at<double>(0, 0)*point.x + H.at<double>(0, 1)*point.y + H.at<double>(0, 2)) /
      (H.at<double>(2, 0)*point.x + H.at<double>(2, 1)*point.y + H.at<double>(2, 2));

    double y = (H.at<double>(1, 0)*point.x + H.at<double>(1, 1)*point.y + H.at<double>(1, 2)) /
      (H.at<double>(2, 0)*point.x + H.at<double>(2, 1)*point.y + H.at<double>(2, 2));

    return cv::Point2d(x, y);
  }

  inline cv::Point2d ApplyProjection6(const cv::Point2d& point, const cv::Mat& H) {
    double x = (H.at<double>(0, 0)*point.x + H.at<double>(0, 1)*point.y + H.at<double>(0, 2));
    double y = (H.at<double>(1, 0)*point.x + H.at<double>(1, 1)*point.y + H.at<double>(1, 2));

    return cv::Point2d(x, y);
  }

  cv::Point2d MinPoint(const cv::Mat& im, const cv::Mat& H, cv::Point2d& max_point) {
    //(0,0)
    cv::Point2d min_point;
    max_point = min_point = ApplyProjection9(cv::Point2d(0, 0), H);

    //(w,0)
    cv::Point2d tmp_point;
    tmp_point = ApplyProjection9(cv::Point2d(im.cols, 0), H);
    min_point.x = std::min(min_point.x, tmp_point.x);
    max_point.x = std::max(max_point.x, tmp_point.x);
    min_point.y = std::min(min_point.y, tmp_point.y);
    max_point.y = std::max(max_point.y, tmp_point.y);

    //(w,h)
    tmp_point = ApplyProjection9(cv::Point2d(im.cols, im.rows), H);
    min_point.x = std::min(min_point.x, tmp_point.x);
    max_point.x = std::max(max_point.x, tmp_point.x);
    min_point.y = std::min(min_point.y, tmp_point.y);
    max_point.y = std::max(max_point.y, tmp_point.y);

    //(0,h)
    tmp_point = ApplyProjection9(cv::Point2d(0, im.rows), H);
    min_point.x = std::min(min_point.x, tmp_point.x);
    max_point.x = std::max(max_point.x, tmp_point.x);
    min_point.y = std::min(min_point.y, tmp_point.y);
    max_point.y = std::max(max_point.y, tmp_point.y);

    return min_point;
  }

  template<typename T>
  void LineOf2Points(T &a, T &b, T &c,T x1, T y1, T x2, T y2){
	  if (fabs(x1 - x2)<0.000001){
		  a = 1.0f;
		  b = 0;
		  c = -x1;
	  }
	  else {
		  a = (y1 - y2) / (x1 - x2);
		  b = -1.0f;
		  c = y1 - a*x1;
	  }
  }

  template <typename T, typename E>
  T FindMinDist(T* a, T* b, T* c, T* inv_a2_plus_b2, int line_num, E x, E y) {
	  T min_dist;
	  for (int i = 0; i < line_num; ++i) {
		  T dist = abs(a[i] * x + b[i] * y + c[i])*inv_a2_plus_b2[i];
		  if (i < 1) {
			  min_dist = dist;
		  }
		  else {
			  min_dist = std::min(min_dist, dist);
		  }
	  }

	  return min_dist;
  }

  void CalculateDistMaps(std::vector<ImageStitchingInfo>& image_stitching_infos, std::vector<float*>& dist_maps) {
	  dist_maps.clear();

	  for (auto& image_stitching_info : image_stitching_infos) {
		  double a[4], b[4], c[4], inv_a2_plus_b2[4];
		  cv::Point2d* corners = image_stitching_info.corners;

		  for (int i = 0; i < 4; ++i) {
			  LineOf2Points(a[i], b[i], c[i], corners[i].x, corners[i].y, corners[(i + 1) % 4].x, corners[(i + 1) % 4].y);
			  inv_a2_plus_b2[i] = 1 / sqrt(a[i] * a[i] + b[i] * b[i]);
		  }

		  cv::Mat& mask = image_stitching_info.mask;
		 
		  int size = mask.cols*mask.rows;
		  float *dist_map = new float[size];
		  memset(dist_map, 0, sizeof(float)*size);
		  uchar* data = mask.data;
		  float max_dist = 0;

		  for (int h = 0; h < mask.rows; ++h) {
			  float* row_dist_map = dist_map + h*mask.cols;
			  int step0 = mask.step[0];
			  uchar* row_data = data + h*mask.step[0];
			  for (int w = 0; w < mask.cols; ++w) {
				  if (row_data[w] == 0) {
					  continue;
				  }

				  row_dist_map[w] = FindMinDist(a, b, c, inv_a2_plus_b2, 4, w, h);
				  max_dist = std::max(max_dist, row_dist_map[w]);
			  }
		  }

		  //normalize dist_map
		  for (int idx = 0; idx < size; ++idx) {
			  dist_map[idx] /= max_dist;
		  }

		  dist_maps.emplace_back(dist_map);
		  image_stitching_info.mask = cv::Mat::zeros(mask.size(), mask.type());
	  }
  }

  void UpdateMask(std::vector<ImageStitchingInfo>& image_stitching_infos, int cols, int rows) {
	  std::vector<float*> dist_maps;
	  CalculateDistMaps(image_stitching_infos, dist_maps);
	  
	  auto FindMaxId = [&dist_maps, &image_stitching_infos](int x, int y) {
		  int id = -1;
		  float dist = 0;

		  int x1 = -1, y1 = -1;
		  for (int i = 0; i < image_stitching_infos.size(); ++i) {
			  const cv::Rect& rect = image_stitching_infos[i].rect;
			  if (x > rect.x && x< rect.x + rect.width && y>rect.y && y < rect.y + rect.height) {
				  cv::Point point = cv::Point(x, y) - cv::Point(rect.x, rect.y);
				  float distance = dist_maps[i][point.y*rect.width + point.x];
				  if (dist_maps[i][point.y*rect.width + point.x] > dist) {
					  dist = dist_maps[i][point.y*rect.width + point.x];
					  id = i;
				  }	  
			  }
		  }
		
		  return id;
	  };

	  //update mask
	  for (int h = 0; h < rows; ++h) {
		  for (int w = 0; w < cols; ++w) {
			  int max_id = FindMaxId(w, h);
			  if (max_id < 0) {
				  continue;
			  }

			  const cv::Rect& rect = image_stitching_infos[max_id].rect;
			  image_stitching_infos[max_id].mask.at<uchar>(h - rect.y, w - rect.x) = 255;
		  }
	  }

  }

  cv::Mat Bleeding(const std::vector<ImageInfo>& image_infos) {
    std::vector<ImageStitchingInfo> image_stitching_infos;
    cv::Point2d global_min_point(0, 0), global_max_point(0, 0);
   
    cv::Mat kernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3));
    for (auto& image_info : image_infos) {
      cv::Mat im;
      if (image_info.image.channels() < 4) {
        im = AddAlphaChannel(image_info.image);
      }
      else {
        im = image_info.image;
      }
      
      cv::Mat H = image_info.homo;
      
	    ImageStitchingInfo image_stitching_info;
	    image_stitching_info.corners[0] = ApplyProjection9(cv::Point2d(0, 0), H);
	    image_stitching_info.corners[1] = ApplyProjection9(cv::Point2d(image_info.image.cols, 0), H);
	    image_stitching_info.corners[2] = ApplyProjection9(cv::Point2d(image_info.image.cols, image_info.image.rows), H);
	    image_stitching_info.corners[3] = ApplyProjection9(cv::Point2d(0, image_info.image.rows), H);

	    cv::Point2d max_point, min_point;

	    for (int i = 0; i < 4; ++i) {
		    global_min_point.x = std::min(image_stitching_info.corners[i].x, global_min_point.x);
		    global_min_point.y = std::min(image_stitching_info.corners[i].y, global_min_point.y);
		    global_max_point.x = std::max(image_stitching_info.corners[i].x, global_max_point.x);
		    global_max_point.y = std::max(image_stitching_info.corners[i].y, global_max_point.y);

		    if (i == 0) {
			    max_point = min_point = image_stitching_info.corners[i];
		    }
		    else {
			    min_point.x = std::min(image_stitching_info.corners[i].x, min_point.x);
			    min_point.y = std::min(image_stitching_info.corners[i].y, min_point.y);
			    max_point.x = std::max(image_stitching_info.corners[i].x, max_point.x);
			    max_point.y = std::max(image_stitching_info.corners[i].y, max_point.y);
		    }
	    }

	    for (int i = 0; i < 4; ++i) {
		    image_stitching_info.corners[i] -= min_point;
	    }
     
	    image_stitching_info.min_point = min_point;
      H.row(0) -= min_point.x*H.row(2);
      H.row(1) -= min_point.y*H.row(2);
      
	    int nw, nh;
      nw = max_point.x - min_point.x + 1;
      nh = max_point.y - min_point.y + 1;
      std::cout << "bleeding: " << H << std::endl;
      cv::warpPerspective(im, image_stitching_info.im, H, cv::Size(nw, nh));
      char image_save_path[512];
      sprintf_s(image_save_path, "D:\\image_stitching_result\\tmp\\%08d.png", image_info.id);
      cv::imwrite(image_save_path, image_stitching_info.im);
      if (!image_info.mask.empty()) {
        cv::warpPerspective(image_info.mask, image_stitching_info.mask, H, cv::Size(nw, nh));
        cv::threshold(image_stitching_info.mask, image_stitching_info.mask, 128, 255, cv::THRESH_BINARY);
      }
      else {
        cv::Mat alpha_channel = GetAlphaChannel(image_stitching_info.im);
        cv::threshold(alpha_channel, image_stitching_info.mask, 128, 255, cv::THRESH_BINARY);
      }
	  
	    image_stitching_infos.emplace_back(image_stitching_info);
    }

    for (int i = 0; i < image_stitching_infos.size(); ++i) {
      cv::Point tmp_point = image_stitching_infos[i].min_point - global_min_point;
      image_stitching_infos[i].rect = cv::Rect(tmp_point.x, tmp_point.y,
        image_stitching_infos[i].im.cols, image_stitching_infos[i].im.rows);
    }

    cv::Point global_size = global_max_point - global_min_point + cv::Point2d(2, 2);

#if 1
    cv::Mat result = cv::Mat(global_size.y, global_size.x, CV_8UC4, cv::Scalar(0, 0, 0, 0));
    for (const auto& image_stitching_info : image_stitching_infos) {
      image_stitching_info.im.copyTo(result(image_stitching_info.rect), image_stitching_info.mask);
    }

    return result;
#else 
	  UpdateMask(image_stitching_infos, global_size.x, global_size.y);
	  cv::detail::MultiBandBlender blender(false, 5);

	  blender.prepare(cv::Rect(0, 0, global_size.x, global_size.y));
	
	  for (const auto& image_stitching_info : image_stitching_infos) {
		  std::vector<cv::Mat> channels;
		  cv::split(image_stitching_info.im, channels);
		  cv::Mat img;
		  channels.resize(3);
		  cv::merge(channels, img);
		  img.convertTo(img, CV_16S);
		  blender.feed(img, image_stitching_info.mask, image_stitching_info.min_point - global_min_point);
	  }

	  cv::Mat result_image, result_mask;
	  blender.blend(result_image, result_mask);
	  cv::Mat result, mask;
	  result_image.convertTo(result, CV_8U);
	  result_mask.convertTo(mask, CV_8U);
	
    return result;
#endif
  }


  cv::Mat ImageRotate(const cv::Mat& img, double angle) {
    cv::Mat M = cv::Mat::zeros(cv::Size(3, 2), CV_64FC1);
    M.at<double>(0, 0) = cos(angle);
    M.at<double>(0, 1) = -sin(angle);
    M.at<double>(1, 0) = sin(angle);
    M.at<double>(1, 1) = cos(angle);
    
    cv::Point2d corners[3];
    corners[0] = ApplyProjection6(cv::Point2d(img.cols, 0), M);
    corners[1] = ApplyProjection6(cv::Point2d(img.cols, img.rows), M);
    corners[2] = ApplyProjection6(cv::Point2d(0, img.rows), M);

    cv::Point2d min_point(0, 0), max_point(0, 0);
    for (int i = 0; i < 3; ++i) {
      min_point.x = std::min(corners[i].x, min_point.x);
      min_point.y = std::min(corners[i].y, min_point.y);
      max_point.x = std::max(corners[i].x, max_point.x);
      max_point.y = std::max(corners[i].y, max_point.y);
    }

    M.at<double>(0, 2) -= min_point.x;
    M.at<double>(1, 2) -= min_point.y;
    
    cv::Mat dst;
    int nw, nh;
    nw = max_point.x - min_point.x + 1;
    nh = max_point.y - min_point.y + 1;
    cv::warpAffine(img, dst, M, cv::Size(nw, nh));
    
    return dst;
  }

  cv::Mat ImageStitchingFunc(std::vector<cv::Mat> images, int idx) {
  }

  cv::Mat ImageWarpAffine(const cv::Mat& image, const cv::Mat& M) {
    cv::Point2d corners[3];
    corners[0] = ApplyProjection6(cv::Point2d(image.cols, 0), M);
    corners[1] = ApplyProjection6(cv::Point2d(image.cols, image.rows), M);
    corners[2] = ApplyProjection6(cv::Point2d(0, image.rows), M);

    cv::Point2d min_point, max_point;
    min_point = max_point = ApplyProjection6(cv::Point2d(0, 0), M);
    for (int i = 0; i < 3; ++i) {
      min_point.x = std::min(min_point.x, corners[i].x);
      min_point.y = std::min(min_point.y, corners[i].y);
      max_point.x = std::max(max_point.x, corners[i].x);
      max_point.y = std::max(max_point.y, corners[i].y);
    }

    cv::Mat M1 = M.clone();
    M1.at<double>(0, 2) -= min_point.x;
    M1.at<double>(1, 2) -= min_point.y;
    int nh, nw;
    nh = max_point.y - min_point.y + 1;
    nw = max_point.x - min_point.x + 1;

    cv::Mat affine_image;
    cv::warpAffine(image, affine_image, M1, cv::Size(nw, nh));

    return affine_image;
  }

  cv::Mat ImageWarpPerspective(const cv::Mat& image, const cv::Mat& H) {
    cv::Point2d corners[3];
    corners[0] = ApplyProjection9(cv::Point2d(image.cols, 0), H);
    corners[1] = ApplyProjection9(cv::Point2d(image.cols, image.rows), H);
    corners[2] = ApplyProjection9(cv::Point2d(0, image.rows), H);

    cv::Point2d min_point, max_point;
    min_point = max_point = ApplyProjection9(cv::Point2d(0, 0), H);
    for (int i = 0; i < 3; ++i) {
      min_point.x = std::min(min_point.x, corners[i].x);
      min_point.y = std::min(min_point.y, corners[i].y);
      max_point.x = std::max(max_point.x, corners[i].x);
      max_point.y = std::max(max_point.y, corners[i].y);
    }

    cv::Mat H1 = H.clone();
    H1.row(0) -= min_point.x*H.row(2);
    H1.row(1) -= min_point.y*H.row(2);
    int nh, nw;
    nh = max_point.y - min_point.y + 1;
    nw = max_point.x - min_point.x + 1;

    cv::Mat affine_image;
    cv::warpPerspective(image, affine_image, H1, cv::Size(nw, nh));

    return affine_image;
  }

  cv::Mat AddAlphaChannel(const cv::Mat& im) {
    std::vector<cv::Mat> channels;
    cv::split(im, channels);
    channels.emplace_back(cv::Mat(im.size(), CV_8UC1, cv::Scalar(255)));

    cv::Mat result;
    cv::merge(channels, result);

    return result;
  }

  cv::Mat DelAlphaChannel(const cv::Mat& im) {
    std::vector<cv::Mat> channels;
    cv::split(im, channels);
    channels.resize(channels.size() - 1);

    cv::Mat result;
    cv::merge(channels, result);

    return result;
  }

  cv::Mat GetAlphaChannel(const cv::Mat& im) {
    std::vector<cv::Mat> channels;
    cv::split(im, channels);
   
    return channels[3];
  }

  void LoadImageInfoFromXml(const std::string& xml_path, std::vector<Image>& images) {
    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load_file(xml_path.c_str());
    pugi::xml_node block = doc.child("BlocksExchange").child("Block");

    pugi::xml_node photo_groups = block.child("Photogroups");
    for (pugi::xml_node photo_group = photo_groups.first_child(); photo_group != NULL; photo_group = photo_group.next_sibling()) {
      double focal_length = photo_group.child("FocalLengthPixels").text().as_double();
      double cx = photo_group.child("PrincipalPoint").child("x").text().as_double();
      double cy = photo_group.child("PrincipalPoint").child("y").text().as_double();
      double k1 = photo_group.child("Distortion").child("K1").text().as_double();
      double k2 = photo_group.child("Distortion").child("K2").text().as_double();
      double k3 = photo_group.child("Distortion").child("K3").text().as_double();
      double p1 = photo_group.child("Distortion").child("P1").text().as_double();
      double p2 = photo_group.child("Distortion").child("P2").text().as_double();

      std::vector<double> dist_coeffs({ k1, k2, p1, p2, k3 });

      cv::Mat K = cv::Mat::zeros(cv::Size(3, 3), CV_64FC1);
#if 1
      K.at<double>(0, 0) = K.at<double>(1, 1) = focal_length;
      K.at<double>(0, 2) = cx;
      K.at<double>(1, 2) = cy;
      K.at<double>(2, 2) = 1;
#elif 1
      K.at<double>(0, 0) = K.at<double>(1, 1) = 3666.666504;
      K.at<double>(0, 2) = 2736;
      K.at<double>(1, 2) = 1824;
      K.at<double>(2, 2) = 1;
#else 
      K.at<double>(0, 0) = K.at<double>(1, 1) = 723.553;
      K.at<double>(0, 2) = 480;
      K.at<double>(1, 2) = 360;
      K.at<double>(2, 2) = 1;
#endif
      for (auto& photo : photo_group.children("Photo")) {
        Image image;
        image.K = K.clone();
        image.dist_coeffs = dist_coeffs;

        image.id = photo.child("Id").text().as_int();
        image.image_path = photo.child("ImagePath").text().as_string();
        image.im = cv::imread(image.image_path, CV_LOAD_IMAGE_UNCHANGED);
        image.R = cv::Mat::zeros(cv::Size(3, 3), CV_64FC1);
        image.c = cv::Mat::zeros(cv::Size(3, 1), CV_64FC1);

        for (int i = 0; i < 3; ++i) {
          for (int j = 0; j < 3; ++j) {
            image.R.at<double>(i, j) = photo.child("Pose").child("Rotation").child((std::string("M_")
              + std::to_string(i) + std::to_string(j)).c_str()).text().as_double();
          }
        }

        image.center(0) = photo.child("Pose").child("Center").child("x").text().as_double();
        image.center(1) = photo.child("Pose").child("Center").child("y").text().as_double();
        image.center(2) = photo.child("Pose").child("Center").child("z").text().as_double();

        std::cout << "load image: " << image.image_path << std::endl;

        images.emplace_back(image);
       /* if (image.id > 10) {
          break;
        }*/
      }
    }
  }

  void LoadImageInfoFromOrb(const std::string& key_frame_path, std::vector<Image>& images) {
    std::string txt_path = key_frame_path + "/KeyFrameTrajectory.txt";
    std::string pic_dir = key_frame_path + "/images/";
    std::string ext = ".jpg";
    std::ifstream fs(txt_path);
    std::string line;
    
    cv::Mat K = cv::Mat::zeros(cv::Size(3, 3), CV_64FC1);
    K.at<double>(0, 0) = K.at<double>(1, 1) = 723.553;
    K.at<double>(0, 2) = 480;
    K.at<double>(1, 2) = 360;
    K.at<double>(2, 2) = 1;

    int id = 0;
    while (std::getline(fs, line)) {
      std::istringstream ss(line);
      Image image;
      Eigen::Vector4d v4; 
      Eigen::Vector3d c3;
      ss >> image.image_path >> image.center(0) >> image.center(1) >> image.center(2) >> v4(0) >> v4(1) >> v4(2) >> v4(3);
      image.image_path = pic_dir + image.image_path + ext;
      image.im = cv::imread(image.image_path);
      Eigen::Quaterniond quat(v4);
      Eigen::Matrix3d R = quat.toRotationMatrix();
      image.R = cv::Mat::zeros(3, 3, CV_64FC1);
      for (int r = 0; r < 3; ++r) {
        for (int c = 0; c < 3; ++c) {
          image.R.at<double>(r, c) = R(r, c);
        }
      }

      image.K = K;
      image.id = id++;

      images.emplace_back(image);
    }
  }

}