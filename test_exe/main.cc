﻿#include<iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <chrono>
#include "bleeding.h"
#include "timer.h"
#include <unordered_map>
#include <Eigen/Eigen>
#include "ImageStitching.h"

using namespace std;
using namespace cv;
using namespace std::chrono;
using namespace AirlookStitching;

void Run0() {
  cv::Mat im;
  char im_path[512];
  cv::Ptr<cv::ORB> orb = cv::ORB::create(1000);
  
  std::vector<ImageInfo> image_infos;
  std::chrono::high_resolution_clock::time_point start_time, end_time;
  double sum_time = 0;
  Timer time;

  time.Start();
  for (int i = 0; i < 100; ++i) {
    time.Pause();
    //sprintf(im_path, "E:\\data\\dg_data\\photo\\X\\dgcsX%04d.jpg", i);
    sprintf(im_path, "D:\\image_stitching_result\\data\\%08d.jpg", i);
    im = imread(im_path, cv::IMREAD_UNCHANGED);

    std::cout << "read image: " << im_path << std::endl;
    
    time.Resume();
    ImageInfo image_info;
    cv::resize(im, image_info.image, cv::Size(), 0.2, 0.2);
    orb->detect(image_info.image, image_info.key_points);
    orb->compute(image_info.image, image_info.key_points, image_info.descriptors);
    image_info.homo = cv::Mat::eye(3, 3, CV_64F);
    image_infos.emplace_back(image_info);
  }

  cv::BFMatcher  bf_matcher(cv::NORM_HAMMING, true);
  for (int i = 1; i < image_infos.size(); ++i) {
    vector<cv::DMatch> matchs;
    bf_matcher.match(image_infos[i - 1].descriptors, image_infos[i].descriptors, matchs);
    vector<cv::Point2f> points1, points2;
    points1.reserve(matchs.size());
    points2.reserve(matchs.size());
    for (const auto& match : matchs) {
      points1.emplace_back(image_infos[i - 1].key_points[match.queryIdx].pt);
      points2.emplace_back(image_infos[i].key_points[match.trainIdx].pt);
    }

    vector<uchar> inlier_mask;
    Mat H = cv::findHomography(points2, points1, RANSAC, 8.0, inlier_mask);
#if 1
	H.at<double>(0, 0) = 1;
	H.at<double>(0, 1) = 0;
	H.at<double>(1, 0) = 0;
	H.at<double>(1, 1) = 1;
	H.at<double>(2, 0) = 0;
	H.at<double>(2, 1) = 0;
#endif
	std::cout << i - 1 << " , " << i << " : " << std::endl << H << std::endl;
    image_infos[i].homo = image_infos[i - 1].homo*H; 
    //image_infos[i].homo /= image_infos[i].homo.at<double>(2, 2);
  }

  std::cout << "image feature cost: " << time.ElapsedSeconds() << " s" << std::endl;

  time.Restart();
  cv::Mat image = Bleeding(image_infos);
  std::cout << "bleeding: " << time.ElapsedSeconds() << " s" << std::endl;
  char image_save_path[512];
  sprintf_s(image_save_path, "D:\\image_stitching_result\\%02d.png", 2);
  cv::imwrite(image_save_path, image);
}

#define PI 3.1415926
void Run1() {
  std::string image_path = "E:\\data\\dg_data\\photo\\X\\dgcsX0005.jpg";
  cv::Mat img = cv::imread(image_path, cv::IMREAD_UNCHANGED);
 
  double angle = 30.f / 180 * PI;
  std::cout << sin(angle) << std::endl;
  cv::Mat dst = ImageRotate(img, angle);

  cv::imwrite("E:\\result_data\\00.png", dst);
}

void Run2() {
  //std::string xml_path = "D:\\cc_test\\herb\\neimeng300.xml";
  std::string xml_path = "D:\\image_stitching_resource\\dajiang\\kongsan\\at.xml";
  std::vector<Image> images;
  LoadImageInfoFromXml(xml_path, images);

#if 1
  cv::Mat K = cv::getOptimalNewCameraMatrix(images[0].K, images[0].dist_coeffs, images[0].im.size(), 1);
  std::cout << "new_matrix: " << K << std::endl;
  std::cout << "K: " << images[0].K << std::endl;
  cv::Mat map1, map2;
  cv::initUndistortRectifyMap(images[0].K, images[0].dist_coeffs, cv::Mat(), K, images[0].im.size(), CV_32FC1, map1, map2);
#endif

  for (auto& image : images) {
    cv::Mat R = image.R;
    std::cout << "K: " << K << std::endl;
    R.at<double>(0, 2) = 0;
    R.at<double>(1, 2) = 0;
    R.at<double>(2, 2) = 1;
    std::cout << "R: " << R << std::endl;

    cv::Mat K_inv;
    cv::invert(K, K_inv);
    std::cout << K*K_inv << std::endl;
    cv::Mat H = K*R.inv()*K_inv;
    std::cout << "H: " << H << std::endl;
    
    cv::Mat img = AddAlphaChannel(image.im);
    cv::Mat image_undisort;
    cv::remap(img, image_undisort, map1, map2, INTER_LINEAR);
    cv::Mat image_rotate = ImageWarpAffine(image_undisort, H.rowRange(0, 2));
    std::cout << "channel: " << image_rotate.channels() << std::endl;
    char image_save_path[512];
    sprintf_s(image_save_path, "D:\\image_stitching_result\\data_10\\%08d.png", image.id);
    cv::imwrite(image_save_path, image_rotate);
  }
}

void Run3() {
  cv::Mat im;
  char im_path[512];
  cv::Ptr<cv::ORB> orb = cv::ORB::create(10000);

  std::vector<ImageInfo> image_infos;
  double sum_time = 0;
  Timer time;

  time.Start();
  //cv::namedWindow("match", 0);
  for (int i = 0; i < 133; ++i) {
    time.Pause();
    //sprintf(im_path, "E:\\data\\dg_data\\photo\\X\\dgcsX%04d.jpg", i);
    sprintf(im_path, "D:\\image_stitching_result\\data_10\\%08d.png", i);
    im = imread(im_path, cv::IMREAD_UNCHANGED);

    std::cout << "read image: " << im_path << std::endl;

    time.Resume();
    ImageInfo image_info;
    image_info.image = DelAlphaChannel(im);
    image_info.mask = GetAlphaChannel(im);

    orb->detect(image_info.image, image_info.key_points, image_info.mask);
    orb->compute(image_info.image, image_info.key_points, image_info.descriptors);
    image_info.homo = cv::Mat::eye(3, 3, CV_64F);
    image_infos.emplace_back(image_info);
  }

  cv::BFMatcher  bf_matcher(cv::NORM_HAMMING, true);
  for (int i = 1; i < image_infos.size(); ++i) {
    vector<cv::DMatch> matchs;
    bf_matcher.match(image_infos[i - 1].descriptors, image_infos[i].descriptors, matchs);
    vector<cv::Point2f> points1, points2;
    points1.reserve(matchs.size());
    points2.reserve(matchs.size());
    for (const auto& match : matchs) {
      points1.emplace_back(image_infos[i - 1].key_points[match.queryIdx].pt);
      points2.emplace_back(image_infos[i].key_points[match.trainIdx].pt);
    }

    vector<uchar> inlier_mask;
    Mat H = cv::findHomography(points2, points1, RANSAC, 8.0, inlier_mask, 10000);

    std::cout << i - 1 << " , " << i << " : " << std::endl << H << std::endl;
    double scale = 1;
#if 0
    const std::vector<cv::KeyPoint> &query_key_points = image_infos[i - 1].key_points;
    const std::vector<cv::KeyPoint> &train_key_points = image_infos[i].key_points;

    cv::Point2f query_point = query_key_points[matchs[0].queryIdx].pt;
    cv::Point2f train_point = train_key_points[matchs[0].trainIdx].pt;
    double query_dist = 0, train_dist = 0;
    for (int j = 1; j < matchs.size(); ++j) {
      cv::Point2f tmp_point = query_key_points[matchs[j].queryIdx].pt - query_point;
      query_dist += abs(tmp_point.x);
      tmp_point = train_key_points[matchs[j].trainIdx].pt - train_point;
      train_dist += abs(tmp_point.x);
    }

    scale = query_dist / train_dist;

    cv::resize(image_infos[i].image, image_infos[i].image, cv::Size(), scale, scale);
    cv::resize(image_infos[i].mask, image_infos[i].mask, cv::Size(), scale, scale);
#endif
    //H = H*scale;
#if 0
    for (int j = 0; j < inlier_mask.size(); ++j) {
      if (!inlier_mask[j]) {
        inlier_mask.erase(inlier_mask.cbegin() + j);
        matchs.erase(matchs.cbegin() + j);
        --j;
      }
    }

    cv::Mat match;
    cv::drawMatches(image_infos[i - 1].image, image_infos[i - 1].key_points, image_infos[i].image, image_infos[i].key_points, matchs, match);
    cv::imshow("match", match);
    cv::waitKey(0);
#endif
   
#if 0
    H.at<double>(0, 0) = 1;
    H.at<double>(0, 1) = 0;
    H.at<double>(1, 0) = 0;
    H.at<double>(1, 1) = 1;
    H.at<double>(2, 0) = 0;
    H.at<double>(2, 1) = 0;
#endif
    //std::cout << "scale: " << scale << std::endl;
    
    image_infos[i].homo = image_infos[i - 1].homo*H;
  }

  std::cout << "image feature cost: " << time.ElapsedSeconds() << " s" << std::endl;

  time.Restart();
  cv::Mat image = Bleeding(image_infos);
  std::cout << "bleeding: " << time.ElapsedSeconds() << " s" << std::endl;
  char image_save_path[512];
  sprintf_s(image_save_path, "D:\\image_stitching_result\\%02d.png", 3);
  cv::imwrite(image_save_path, image);
}

void Run4() {
  cv::Mat im;
  char im_path[512];
  cv::Ptr<cv::ORB> orb = cv::ORB::create(3000);

  std::vector<ImageInfo> image_infos;
  double sum_time = 0;
  Timer time;

  time.Start();
  for (int i = 0; i < 25; ++i) {
    time.Pause();
    sprintf(im_path, "D:\\image_stitching_result\\data\\%08d.png", i);
    im = imread(im_path, cv::IMREAD_UNCHANGED);

    std::cout << "read image: " << im_path << std::endl;

    time.Resume();
    ImageInfo image_info;
    image_info.image = DelAlphaChannel(im);
    image_info.mask = GetAlphaChannel(im);

    orb->detect(image_info.image, image_info.key_points, image_info.mask);
    orb->compute(image_info.image, image_info.key_points, image_info.descriptors);
    image_info.homo = cv::Mat::eye(3, 3, CV_64F);
    image_infos.emplace_back(image_info);
  }

  cv::BFMatcher  bf_matcher(cv::NORM_HAMMING, true);
  std::vector<ImageInfo> bleeding_infos;
  bleeding_infos.emplace_back(image_infos[0]);
  for (int i = 1; i < image_infos.size(); ++i) {
    vector<cv::DMatch> matchs;
    bf_matcher.match(bleeding_infos[0].descriptors, image_infos[i].descriptors, matchs);
    vector<cv::Point2f> points1, points2;
    points1.reserve(matchs.size());
    points2.reserve(matchs.size());
    for (const auto& match : matchs) {
      points1.emplace_back(bleeding_infos[0].key_points[match.queryIdx].pt);
      points2.emplace_back(image_infos[i].key_points[match.trainIdx].pt);
    }
    std::cout << "image: " << i << "\t matchs: " << matchs.size() << std::endl;
    vector<uchar> inlier_mask;
    Mat H = cv::findHomography(points2, points1, RANSAC, 3.0, inlier_mask);

    for (int j = 0; j < inlier_mask.size(); ++j) {
      if (!inlier_mask[j]) {
        inlier_mask.erase(inlier_mask.cbegin() + j);
        matchs.erase(matchs.cbegin() + j);
        --j;
      }
    }

    char image_save_path[512];
    cv::Mat match;
    cv::drawMatches(bleeding_infos[0].image, bleeding_infos[0].key_points, image_infos[i].image, image_infos[i].key_points, matchs, match);
    sprintf_s(image_save_path, "D:\\image_stitching_result\\%04d.png", i);
    cv::imwrite(image_save_path, match);
    std::cout << "image: " << i << "\t inlier_match: " << inlier_mask.size() << std::endl << "H: " << std::endl << H << std::endl << std::endl;
 
#if 0
    H.at<double>(0, 0) = 1;
    H.at<double>(0, 1) = 0;
    H.at<double>(1, 0) = 0;
    H.at<double>(1, 1) = 1;
    H.at<double>(2, 0) = 0;
    H.at<double>(2, 1) = 0;
#endif
   
    image_infos[i].homo = H;

    bleeding_infos.emplace_back(image_infos[i]);

    cv::Mat image = Bleeding(bleeding_infos);

    ImageInfo image_info;
    image_info.image = DelAlphaChannel(image);
    image_info.mask = GetAlphaChannel(image);
    image_info.homo = cv::Mat::eye(3, 3, CV_64F);

    orb->detect(image_info.image, image_info.key_points, image_info.mask);
    orb->compute(image_info.image, image_info.key_points, image_info.descriptors);

    bleeding_infos.clear();
    bleeding_infos.emplace_back(image_info);
    
    
    sprintf_s(image_save_path, "D:\\image_stitching_result\\%02d.png", i);
    cv::imwrite(image_save_path, image);
    
  }
}

void Run5() {
  std::string xml_path = "D:\\image_stitching_resource\\dajiang\\kongsan\\at.xml";
  std::vector<Image> images;
  LoadImageInfoFromXml(xml_path, images);



  cv::Ptr<cv::ORB> orb = cv::ORB::create(3000);
  cv::Ptr<cv::xfeatures2d::SURF> surf = cv::xfeatures2d::SURF::create(300);
  cv::Ptr<cv::xfeatures2d::SIFT> sift = cv::xfeatures2d::SIFT::create(30000);

  std::vector<std::pair<int, int>> air_strips;
  std::vector<std::vector<Eigen::Vector3d>> air_strips_centers;

  auto InitAirStrips = [&images](std::vector<std::pair<int, int>>& air_strips,
    std::vector<std::vector<Eigen::Vector3d>>& air_strips_centers) {
    air_strips.emplace_back(0, 20);
    air_strips.emplace_back(23, 41);
    air_strips.emplace_back(44, 62);
    air_strips.emplace_back(65, 83);
    air_strips.emplace_back(86, 104);
    air_strips.emplace_back(107, 126);
    air_strips.emplace_back(129, 148);
    air_strips.emplace_back(151, 170);
    air_strips.emplace_back(173, 193);

    for (int i = 0; i < air_strips.size(); ++i) {
      std::vector<Eigen::Vector3d> air_strip_centers;
      for (int j = air_strips[i].first; j <= air_strips[i].second; ++j) {
        air_strip_centers.emplace_back(images[j].center);
      }
      air_strips_centers.emplace_back(air_strip_centers);
    }
  };

  InitAirStrips(air_strips, air_strips_centers);
  
  auto GetStripId = [&air_strips](int image_id) {
    for (int i = 0; i < air_strips.size(); ++i) {
      if (image_id >= air_strips[i].first && image_id <= air_strips[i].second) {
        return i;
      }
    }

    return -1;
  };

  char im_path[512];
  std::map<int, ImageInfo> image_infos;
  for (int i = 0; i < 196; ++i) {
    if (GetStripId(i) < 0) {
      continue;
    }
    sprintf(im_path, "D:\\image_stitching_result\\data_10\\%08d.png", i);
    cv::Mat im = imread(im_path, cv::IMREAD_UNCHANGED);

    std::cout << "read image: " << im_path << std::endl;

    ImageInfo image_info;
    image_info.image = im;
    image_info.mask = GetAlphaChannel(im);
    image_info.id = i;

#define SURF1
#if defined(ORB1)
    orb->detect(image_info.image, image_info.key_points, image_info.mask);
    orb->compute(image_info.image, image_info.key_points, image_info.descriptors);
#elif defined(SURF1)
    surf->detect(image_info.image, image_info.key_points, image_info.mask);
    surf->compute(image_info.image, image_info.key_points, image_info.descriptors);
#else
    sift->detect(image_info.image, image_info.key_points, image_info.mask);
    sift->compute(image_info.image, image_info.key_points, image_info.descriptors);
#endif
    image_info.homo = cv::Mat::eye(3, 3, CV_64F);
    image_infos[i] = image_info;
  }

  auto GetNearestImageId = [&air_strips, &air_strips_centers](int strip_id, Eigen::Vector3d& center) {
    const auto& air_strip_centers = air_strips_centers[strip_id];

    int result_id;
    double distance = std::numeric_limits<double>::max();
    for (int i = 0; i < air_strip_centers.size(); ++i) {
      double d = (center - air_strip_centers[i]).head(2).norm();
      if (distance > d) {
        result_id = i;
        distance = d;
      }
    }

    return result_id + air_strips[strip_id].first;
  };

#if defined(ORB1)
  cv::BFMatcher  bf_matcher(cv::NORM_HAMMING, true);
#else
  cv::BFMatcher  bf_matcher(cv::NORM_L2, true);
#endif
  auto FindHomo = [&bf_matcher](const ImageInfo&image_info1, const ImageInfo& image_info2) {
    vector<cv::DMatch> matchs;
    bf_matcher.match(image_info1.descriptors, image_info2.descriptors, matchs);
    vector<cv::Point2f> points1, points2;
    points1.reserve(matchs.size());
    points2.reserve(matchs.size());
    for (const auto& match : matchs) {
      points1.emplace_back(image_info1.key_points[match.queryIdx].pt);
      points2.emplace_back(image_info2.key_points[match.trainIdx].pt);
    }

    vector<uchar> inlier_mask;
    Mat H = cv::findHomography(points2, points1, RANSAC, 8.0, inlier_mask, 10000);

#if 0
    int match_num = matchs.size();
    for (int j = 0; j < inlier_mask.size(); ++j) {
      if (!inlier_mask[j]) {
        inlier_mask.erase(inlier_mask.cbegin() + j);
        matchs.erase(matchs.cbegin() + j);
        --j;
      }
    }

    int inlier_match_num = inlier_mask.size();
    std::cout << "match_num: " << match_num << "\tinlier_match_num: " << inlier_match_num << "\t scale: " << float(inlier_match_num) / match_num << std::endl;

    cv::Mat match;
    cv::Mat im1, im2;
    if (image_info1.image.channels() == 4) {
      im1 = DelAlphaChannel(image_info1.image);
      im2 = DelAlphaChannel(image_info2.image);
    }
    else {
      im1 = image_info1.image;
      im2 = image_info2.image;
    }
    cv::namedWindow("match", 0);
    cv::drawMatches(im1, image_info1.key_points, im2, image_info2.key_points, matchs, match);
    cv::imshow("match", match);
    cv::waitKey(0);
#endif

    return H;
  };
  
  for (auto& image_info : image_infos) {
    int id = image_info.first;
    auto& image = image_info.second;
    
    if (id == 0) {
      continue;
    }

    int strip_id = GetStripId(id);
    int prev_id = id - 1;
    if (strip_id > 0) {
      prev_id = GetNearestImageId(strip_id - 1, images[id].center);
    }
    Mat H = FindHomo(image_infos[prev_id], image_infos[id]);
    std::cout << "id: " << prev_id << " , " << id << std::endl << H << std::endl;

#if 0
    H.at<double>(0, 0) = 1;
    H.at<double>(0, 1) = 0;
    H.at<double>(1, 0) = 0;
    H.at<double>(1, 1) = 1;
    H.at<double>(2, 0) = 0;
    H.at<double>(2, 1) = 0;
    H.at<double>(2, 2) = 1;
#endif
    image_infos[id].homo = image_infos[prev_id].homo*H;
    std::cout << "id: " << id << std::endl << image_infos[id].homo << std::endl;
  }

  std::vector<ImageInfo> image_infos1;
  for (auto& image_info : image_infos) {
    image_infos1.emplace_back(image_info.second);
  }
  cv::Mat image = Bleeding(image_infos1);
  char image_save_path[512];
  sprintf_s(image_save_path, "D:\\image_stitching_result\\%02d.png", 3);
  cv::imwrite(image_save_path, image);
}

void Run6() {
  std::string xml_path = "D:\\image_stitching_resource\\dajiang\\kongsan\\at.xml";
  std::vector<Image> images;
  LoadImageInfoFromXml(xml_path, images);

  char im_path[512];
  std::vector<ImageInfo> image_infos;
  double sum_time = 0;
  Timer time;

  time.Start();
  cv::Mat center = images[0].c;
  for (int i = 0; i < 25; ++i) {
    time.Pause();
    sprintf(im_path, "D:\\image_stitching_result\\data\\%08d.png", i);
    cv::Mat im = imread(im_path, cv::IMREAD_UNCHANGED);

    std::cout << "read image: " << im_path << std::endl;

    time.Resume();
    ImageInfo image_info;
    image_info.image = DelAlphaChannel(im);
    image_info.mask = GetAlphaChannel(im);

    image_info.homo = cv::Mat::eye(3, 3, CV_64F);

    double scale = center.at<double>(0, 2) / images[i].c.at<double>(0, 2);
    cv::Mat tmp = images[i].c*scale;

    image_infos.emplace_back(image_info);
  }
}

void Run7() {
  std::string key_frame_path = "D:/image_stitching_resource/keyFrames_2";
  std::vector<Image> images;
  LoadImageInfoFromOrb(key_frame_path, images);

  for (auto& image : images) {
    cv::Mat R = image.R;
    std::cout << "K: " << image.K << std::endl;
    R.at<double>(0, 2) = 0;
    R.at<double>(1, 2) = 0;
    R.at<double>(2, 2) = 0;
    std::cout << "R: " << R << std::endl;

    cv::Mat K_inv;
    cv::invert(image.K, K_inv);
    std::cout << image.K*K_inv << std::endl;
    cv::Mat H = image.K*R*K_inv;
    std::cout << "H: " << H << std::endl;

    cv::Mat img = AddAlphaChannel(image.im);
   
    cv::Mat image_rotate = ImageWarpAffine(img, H.rowRange(0, 2));
    std::cout << "channel: " << image_rotate.channels() << std::endl;
    char image_save_path[512];
    sprintf_s(image_save_path, "D:\\image_stitching_result\\%08d.png", image.id);
    cv::imwrite(image_save_path, image_rotate);
  }
}

void Run8() {
  //std::string xml_path = "D:\\cc_test\\herb\\neimeng300.xml";
  std::string xml_path = "D:\\image_stitching_resource\\dajiang\\kongsan\\dajiang193_1.xml";
  std::vector<Image> images;
  LoadImageInfoFromXml(xml_path, images);

  for (auto& image : images) {
    cv::Mat R = image.R;
    cv::Mat R_inv = R.inv();
    std::cout << "K: " << image.K << std::endl;
    R.at<double>(2, 0) = 0;
    R.at<double>(2, 1) = 0;
    R.at<double>(2, 2) = 1;
    std::cout << "R: " << R << std::endl;

    std::cout << "R_inv: " << R_inv << std::endl;
    cv::Mat H1 = R*R_inv;
    std::cout << "H1: " << H1 << std::endl;
    cv::Mat K_inv;
    cv::invert(image.K, K_inv);
    std::cout << image.K*K_inv << std::endl;
    cv::Mat H = image.K*R*K_inv;
  
    std::cout << "H: " << H << std::endl;

    cv::Mat img = AddAlphaChannel(image.im);
    cv::Mat image_rotate = ImageWarpAffine(img, R_inv.rowRange(0, 2));
    char image_save_path[512];
    sprintf_s(image_save_path, "D:\\image_stitching_result\\data_10\\%08d.png", image.id);
    cv::imwrite(image_save_path, image_rotate);
    std::cout << image_save_path << std::endl;
  }
}

void Run9() {
  std::string xml_path = "D:\\image_stitching_resource\\dajiang\\kongsan\\DJZT_enu.xml";
  std::vector<shared_ptr<AirLookUAV::ImageInfo>> image_infos;
  AirLookUAV::Point4Pos point4_pos;
  AirLookUAV::ImageStitching image_stitching;
  image_stitching.LoadImageInfoFromXml(xml_path, image_infos);
  cv::Mat image;

  int idx = 0;
  for (auto& image_info : image_infos) {
    image = image_stitching.Stitching(image_info, point4_pos);
    ++idx;
    char image_save_path[512];
    sprintf_s(image_save_path, "D:\\image_stitching_result\\result10\\%02d.png", image_info->id);
    cv::imwrite(image_save_path, image);

    char tiff_save_path[512];
    sprintf_s(tiff_save_path, "D:\\image_stitching_result\\result10\\%02d.tif", image_info->id);
#if 0
    if (idx > 2) {
      image_stitching.GenerateTiff(image_save_path, tiff_save_path);
      continue;
    }
#endif
   
    image_stitching.GenerateTiff(image_save_path, point4_pos, tiff_save_path);
  }
  

  //std::stringstream ss;
  ////ss.precision(15);
  ////ss << point4_pos.left_bottom(0) << " , " << point4_pos.left_bottom(1) << std::endl;
  //std::cout.precision(15);
  //std::cout << "left_bottom: " << point4_pos.left_bottom << std::endl;
  //std::cout << "right_top: " << point4_pos.right_top << std::endl;
  //char image_save_path[512];
  //sprintf_s(image_save_path, "D:\\image_stitching_result\\%02d.png", 2);
  //cv::imwrite(image_save_path, image);
}

void Run10() {
  char im_path1[512];
  char im_path2[512];
  cv::Ptr<cv::ORB> orb = cv::ORB::create(10000);
  cv::BFMatcher  bf_matcher(cv::NORM_HAMMING, true);

  std::vector<ImageInfo> image_infos;
  std::chrono::high_resolution_clock::time_point start_time, end_time;
  double sum_time = 0;
  cv::namedWindow("match", 0);
  for (int i = 0; i < 100; ++i) {
    sprintf(im_path1, "D:\\personal\\20200704Image\\image1\\%04d.jpg", i);
    sprintf(im_path2, "D:\\personal\\20200704Image\\image2\\%04d.jpg", i);
    
    cv::Mat im1 = imread(im_path1, cv::IMREAD_UNCHANGED);
    cv::Mat im2 = imread(im_path2, cv::IMREAD_UNCHANGED);

    auto Feature = [&orb](cv::Mat& im, ImageInfo& image_info) {
      cv::resize(im, image_info.image, cv::Size(), 0.2, 0.2); 
      orb->detect(image_info.image, image_info.key_points);
      orb->compute(image_info.image, image_info.key_points, image_info.descriptors);
      image_info.homo = cv::Mat::eye(3, 3, CV_64F);
    };

    ImageInfo image_info1, image_info2;
    
    Feature(im1, image_info1);
    Feature(im2, image_info2);

    auto Match = [&bf_matcher](ImageInfo& image_info1, ImageInfo& image_info2) {
      vector<cv::DMatch> matchs;
      bf_matcher.match(image_info1.descriptors, image_info2.descriptors, matchs);

      vector<cv::Point2f> points1, points2;
      points1.reserve(matchs.size());
      points2.reserve(matchs.size());
      for (const auto& match : matchs) {
        points1.emplace_back(image_info1.key_points[match.queryIdx].pt);
        points2.emplace_back(image_info2.key_points[match.trainIdx].pt);
      }

      vector<uchar> inlier_mask;
      Mat H = cv::findHomography(points2, points1, RANSAC, 8.0, inlier_mask);
      for (int i = 0; i < inlier_mask.size(); ++i) {
        if (!inlier_mask[i]) {
          inlier_mask.erase(inlier_mask.begin() + i);
          matchs.erase(matchs.begin() + i);
          --i;
        }
      }

      cv::Mat match_image;
      cv::drawMatches(image_info1.image, image_info1.key_points, image_info2.image, image_info2.key_points, matchs, match_image);
      
      cv::imshow("match", match_image);
      cv::waitKey(0);

      image_info2.homo = image_info1.homo*H;
    };

    Match(image_info1, image_info2);

    image_infos.clear();
    image_infos.emplace_back(image_info1);
    image_infos.emplace_back(image_info2);

    cv::Mat image = Bleeding(image_infos);
    char image_save_path[512];
    sprintf_s(image_save_path, "D:\\personal\\20200704Image\\%02d.png", i);
    cv::imwrite(image_save_path, image);
    std::cout << "complete: " << image_save_path << std::endl;
  }
}

template<typename T>
void FeatureExtract(ImageInfo& image_info, const T& extractor) {
  extractor->detect(image_info.image, image_info.key_points);
  extractor->compute(image_info.image, image_info.key_points, image_info.descriptors);
}


void Run11() {
  char im_path1[512] = "F:\\ARdata\\NanMen1-High\\photo\\1\\00000308.jpg";
  char im_path2[512] = "F:\\ARdata\\NanMen1-High\\photo\\1\\00000309.jpg";
  

  /*char im_path1[512] = "F:\\ARdata\\VID_20200928_102456_00_003\\fov80result6\\00000270_1.jpg";
  char im_path2[512] = "F:\\ARdata\\VID_20200928_102456_00_003\\fov80result6\\00000290_1.jpg";*/

  //char im_path1[512] = "E:\\data\\HR069\\photo\\QS\\HR69QS0010035.jpg";
  //char im_path2[512] = "E:\\data\\HR069\\photo\\QS\\HR69QS0010038.jpg";

  //char im_path1[512] = "F:\\ARdata\\VID_20200928_102456_00_003\\fov80result8\\00000020_1.jpg";
  //char im_path2[512] = "F:\\ARdata\\VID_20200928_102456_00_003\\fov80result8\\00000030_1.jpg";

  /*char im_path1[512] = "F:\\ARdata\\10-15\\second\\00000454.jpg";
  char im_path2[512] = "F:\\ARdata\\10-15\\second\\00000456.jpg";*/

  /*char im_path1[512] = "F:\\ARdata\\longde-data\\AR相机\\3\\image-000455_3.jpg";
  char im_path2[512] = "F:\\ARdata\\longde-data\\AR相机\\3\\image-000470_3.jpg";*/
  /*char im_path1[512] = "F:\\ARdata\\longde-data\\无人机\\0\\DJI_0929.JPG";
  char im_path2[512] = "F:\\ARdata\\longde-data\\无人机\\0\\DJI_0930.JPG";*/
//#define ORB1
#if defined(ORB1)
  cv::Ptr<cv::ORB> extractor = cv::ORB::create(10000);
#elif defined (SURF)
  cv::Ptr<cv::xfeatures2d::SURF> extractor = cv::xfeatures2d::SURF::create(300);
#else
  cv::Ptr<cv::xfeatures2d::SIFT> extractor = cv::xfeatures2d::SIFT::create(4000);
#endif

#if defined(ORB1)
  cv::Ptr<cv::BFMatcher>  bf_matcher = cv::BFMatcher::create(cv::NORM_HAMMING, true);
#else
  cv::Ptr<cv::BFMatcher>  bf_matcher = cv::BFMatcher::create(cv::NORM_L2, true);
#endif
  auto Match = [&bf_matcher](ImageInfo& image_info1, ImageInfo& image_info2) {
    vector<cv::DMatch> matchs;
    bf_matcher->match(image_info1.descriptors, image_info2.descriptors, matchs);

    vector<cv::Point2f> points1, points2;
    points1.reserve(matchs.size());
    points2.reserve(matchs.size());
    for (const auto& match : matchs) {
      points1.emplace_back(image_info1.key_points[match.queryIdx].pt);
      points2.emplace_back(image_info2.key_points[match.trainIdx].pt);
    }

    vector<uchar> inlier_mask;
    //Mat H = cv::findHomography(points2, points1, RANSAC, 8.0, inlier_mask);

#if 1
    Mat F = cv::findFundamentalMat(points2, points1, inlier_mask, RANSAC, 10);
    for (int i = 0; i < inlier_mask.size(); ++i) {
      if (!inlier_mask[i]) {
        inlier_mask.erase(inlier_mask.begin() + i);
        matchs.erase(matchs.begin() + i);
        --i;
      }
    }
#endif
    return matchs;
  };

  ImageInfo image_info1, image_info2;
  image_info1.image = imread(im_path1, cv::IMREAD_UNCHANGED);
  image_info2.image = imread(im_path2, cv::IMREAD_UNCHANGED);

  //cv::resize(image_info1.image, image_info1.image, cv::Size(), 2, 2);
  //cv::resize(image_info2.image, image_info2.image, cv::Size(), 2, 2);
  FeatureExtract(image_info1, extractor);
  FeatureExtract(image_info2, extractor);

  vector<cv::DMatch> matchs = Match(image_info1, image_info2);
  cv::Mat match;
  std::cout << "match size：" << matchs.size() << std::endl;
  cv::drawMatches(image_info1.image, image_info1.key_points, image_info2.image, image_info2.key_points, matchs, match);

  cv::Mat image1, image2;
  cv::drawKeypoints(image_info1.image, image_info1.key_points, image1);
  cv::drawKeypoints(image_info2.image, image_info2.key_points, image2);
  cv::namedWindow("image1", 0);
  cv::namedWindow("image2", 0);
  cv::imshow("image1", image1);
  cv::imshow("image2", image2);
  std::cout << "image1 keypoint: " << image_info1.key_points.size() << "\t image size: " << image_info1.image.size() << std::endl;
  std::cout << "image2 keypoint: " << image_info2.key_points.size() << "\t image size: " << image_info2.image.size() << std::endl;

  //std::cout << "match : " << matchs.size() << std::endl;
  cv::namedWindow("match", 0);
  cv::imshow("match", match);
  cv::waitKey(0);
}
int main(int argc, char** argv)
{
  Run11();
}